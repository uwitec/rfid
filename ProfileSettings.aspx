﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Homepage.master" AutoEventWireup="true" CodeFile="ProfileSettings.aspx.cs" Inherits="ProfileSettings" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <div id="content-box">
		<div class="padding">
			<div class="clr"></div>
			<div id="new-account-box" class="login">
				<div id="border-top" class="login">
		<div>
    	  <div >
		    <h1>Edit Account</h1>
          </div>
		</div>
	</div>
				<div class="m wbg">
							<div id="account-field-wrapper">
	<fieldset class="loginform">
	<table>
<tr>
<td><b>Firstname:</b></td><td>
    <asp:TextBox ID="txt_Firstname" runat="server" Width="150px"></asp:TextBox></td>
</tr>
<tr>
<td><b>Lastname:</b></td><td>
    <asp:TextBox ID="txt_Lastname" runat="server" Width="150px"></asp:TextBox></td>
</tr>
    <tr>
        <td>
            <b>Username:</b></td>
        <td>
            <asp:TextBox ID="txt_Username" runat="server" Width="250px"></asp:TextBox>
        </td>
    </tr>
<tr>
<td><b>Email:</b></td><td>
    <asp:TextBox ID="txt_email" runat="server" Width="250px"></asp:TextBox>
    </td>
</tr>
</table>
<div id="submit-form-profilebar"><asp:ImageButton ID="Imgbtn_profile" runat="server" 
        ImageUrl="~/images/process-request-button.jpg" onclick="Imgbtn_profile_Click" /></div>
    <div align="center">
    <em><asp:Label ID="lbl_confirmprofile" runat="server"></asp:Label></em>
</div>
				<div class="clr"></div>
		
	</fieldset>
		   </div> 
		   <div id="account-description"></div>
  	<div class="width-45 fltlft">
    <h3> Add/Change Role(s)</h3>
		<fieldset><%--<fieldset class="adminform">--%>
					
			<ul class="creatacclist">
            		
							<li><h4>Role(s):</h4></li>
                           <li>  <asp:CheckBoxList ID="Chk_Roles" runat="server" AutoPostBack="True" 
                                   CssClass="createacchkbox">
        <asp:ListItem Value="1">Order Loan Kit</asp:ListItem>
        <asp:ListItem>Process Loan Kit in and out</asp:ListItem>
        <asp:ListItem>Financial Recording</asp:ListItem>
    </asp:CheckBoxList>
                    <h4>Hospital/Campus:</h4>
                         <asp:DropDownList ID="ddl_Hospital" runat="server" Width="200px" 
        AppendDataBoundItems="True" AutoPostBack="True">
        <asp:ListItem></asp:ListItem>
    </asp:DropDownList></li>
                  </ul>	
                  <div class="clr"></div>
                 <div id="submit-form-profilebar"> <asp:ImageButton ID="Imgbtn_rolesrequest" runat="server" 
        ImageUrl="~/images/process-request-button.jpg" onclick="Imgbtn_rolesrequest_Click" /></div>
       	</fieldset>
	    </div>
          <div>
	
    <div class="width-45 fltlft">
    <div class="pane-slider content">		
  
    <h3>Change Password</h3>
    <fieldset><%--<fieldset class="adminform">--%>
   
				<ul class="adminformlist">
			      <li><h4>Password</h4>
                          <table>
<tr>
<td><b>Current Password:</b></td><td>
    <asp:TextBox ID="txt_Pwd" runat="server" TextMode="Password"></asp:TextBox></td><td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator_oldpwd" runat="server" 
            ErrorMessage="Enter Current Password" ControlToValidate="txt_Pwd" 
            ValidationGroup="pwd">*</asp:RequiredFieldValidator>
    </td>
</tr>
    <tr>
        <td>
          <b>New Password:</b></td>
        <td>
            <asp:TextBox ID="txt_NewPwd" runat="server" TextMode="Password"></asp:TextBox>
        </td><td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator_newpwd" runat="server" 
                ErrorMessage="Enter New Password" ValidationGroup="pwd" Text="*" 
                ControlToValidate="txt_NewPwd"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator_pwds" runat="server" 
                ErrorMessage="Passwords doesn`t match" ControlToCompare="txt_NewPwd" 
                ControlToValidate="txt_Cpwd" ValidationGroup="pwd">*</asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
           <b>Confirm Password:</b></td>
        <td>
            <asp:TextBox ID="txt_Cpwd" runat="server" TextMode="Password"></asp:TextBox>
        </td>
        <td>
            
        </td>
    </tr>
</table>
<div id="submit-form-profilebar"><asp:ImageButton ID="Imgbtn_pwd" runat="server" 
                ImageUrl="~/images/process-request-button.jpg" 
                ValidationGroup="pwd" onclick="Imgbtn_pwd_Click" /></div>
<div align="center">
   <em><asp:Label ID="lbl_PwdSummary" runat="server"></asp:Label></em>
</div>
<div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="pwd" />
    </div>
                  </li>
			  </ul>
			</fieldset>			

	</div>
          
			</div>
</div>
					
					<div class="clr"></div>
                                		
			  </div>
              
       
     
	</div>
	</div>
     </div>			
</asp:Content>

