﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

public partial class Login : System.Web.UI.Page
{
    RFIDDataContext orfid = new RFIDDataContext();
    RfidClass orfidclass = new RfidClass();
    RfidAuthentication oAuthentication=new RfidAuthentication();
    RFID ClassRFID = new RFID();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsPostBack == false)
            {
                ClassRFID.StopReader();
                div_msgsuccess.Visible = false;
                div_msgerror.Visible = false;
                //Call javascript to inspect a phrase for a Barcode
                txt_Username.Attributes.Add("onblur", "javascript:BarcodeLogin(" + txt_Username.ClientID + ")");
                txt_Username.Focus();
                if (Request.QueryString["muser"] != null)
                {
                    loginbox.Visible = false;
                    Panel_multiHosp.Visible = true;
                    ModalPopupExtender1.Show();
                    txt_Username.Text = Session["Multi_Username"].ToString();
                    Txt_Password.Text = Session["Multi_password"].ToString();
                    int accId = 0;
                    Int32.TryParse(Session["AccId"].ToString(), out accId);
                    Txt_Password.TextMode = TextBoxMode.Password;
                    var HospRoles = from tbroles in orfid.tblImplantAccountRoles
                                    join tblacc in orfid.tblImplantAccounts on tbroles.arAccId equals tblacc.accId
                                    join tbhosp in orfid.tblImplantHospitals on tbroles.arHospId equals tbhosp.hospId
                                    where tblacc.accId == accId && (tbroles.arInactive == 'N' || tbroles.arInactive == null)
                                    select new { Hsopname = tbhosp.hospName, HospID = tbhosp.hospId };
                    // we should also add password.   

                    ddl_loginhosp.DataSource = HospRoles.Distinct();
                    ddl_loginhosp.DataTextField = "Hsopname"; ;
                    ddl_loginhosp.DataValueField = "HospID";
                    ddl_loginhosp.DataBind();
                }

               // Authentication();//Must be removed before production upload
            }
            if (Request.QueryString["qy"] != null)
            {
                if (Request.QueryString["qy"] != "")
                {
                    div_msgerror.Visible =false;
                    div_msgsuccess.Visible = true;
                    lbl_banner.Text = "Your request has been sent to the"+"  "+ Request.QueryString["qy"].ToString()+" " + "administrator. An email will be sent to you once approved";
                }
                
            }
            if (Request.QueryString["InvHosp"] != null)
            {
                div_msgerror.Visible = true;
                div_msgsuccess.Visible = false;
                lbl_verify.Text = "Select Hospital";
            }
        }
        catch (Exception ELoginload)
        {
            orfid.Usp_WebErrors("Login.aspx", "Page Load", DateTime.Now.AddDays(0), ELoginload.Message.ToString());
        } 
    }
    
    protected void lnk_newuser_Click(object sender, EventArgs e)
    {
        Response.Redirect("Users.aspx?Signup=newuser"); 
    }
    protected void lnk_forgotpwd_Click(object sender, EventArgs e)
    {
        try
        {
            // If a User Forgets his/her password a New Password Will be created and resent to the User
            if (txt_Username.Text == "")
            {
                div_msgsuccess.Visible = false;
                div_msgerror.Visible = true;
                lbl_verify.Text = "Enter your Username";
                return;
            }
            else
            {
                string NewPwd = "";
                var Reissuepwd = orfid.rp_implantAccountReissuePassword(txt_Username.Text);
                foreach (var RePwd in Reissuepwd)
                {
                    if (RePwd.NewRandomPassword != null)
                        NewPwd = RePwd.NewRandomPassword.ToString();
                }
                if (NewPwd != "")
                {
                    RfidClass ORfidclass = new RfidClass();
                    ORfidclass.MailRequests(txt_Username.Text, ConfigurationManager.AppSettings["SystemAdminAddress"].ToString(), "Changed Password", "Your Password has been reset and your New Password is:" + " " + NewPwd.ToString());
                    div_msgerror.Visible = false;
                    div_msgsuccess.Visible = true;
                    lbl_banner.Text = "Your new Password has been Emailed";
                }
                else
                {
                    div_msgerror.Visible = true;
                    div_msgsuccess.Visible = false;
                    lbl_verify.Text = "Username does not have an account";
                }
            }
        }
        catch (Exception EForgotPassword)
        {
            orfid.Usp_WebErrors("Login.aspx", "Forgot Password", DateTime.Now.AddDays(0), EForgotPassword.Message.ToString());
        }
    }
    protected void lnkbtn_login_Click(object sender, EventArgs e)
    { 
        Authentication();
    }

    private void Authentication()
    {
        try
        {
            string[] LoginAuthentication;
            char CAuthenticated;
            string strAdmin = "";
                      
             LoginAuthentication = oAuthentication.Authentication(txt_Username.Text, Txt_Password.Text);
                CAuthenticated = char.Parse(LoginAuthentication[0].ToString());
                strAdmin = LoginAuthentication[1];
            
            if (CAuthenticated == 'M')
            {
                Session["Multi_Username"] = txt_Username.Text;
                Session["Multi_password"] = Txt_Password.Text;
                Response.Redirect("Login.aspx?muser=Y");
                
            }
            else
            {
                if (CAuthenticated != 'Y')
                {
                    div_msgerror.Visible = true;
                    div_msgsuccess.Visible = false;
                    lbl_verify.Text = "Please Check Username/Password";
                }
                else//checking requests if he is an admin
                {
                    Authenticated(strAdmin);

                }
            }
        }
        catch (Exception Exlogin)
        {
            //orfid.Usp_WebErrors("Login.aspx", "Imgbtn_login_Click", DateTime.Now.AddDays(0), Exlogin.Message.ToString());
        }
        
    }

    private void Authenticated(string strAdmin)
    {
        if (Session["Multi_Username"] != null)
            Session.Remove("Multi_Username");
        if (Session["Multi_password"] != null)
            Session.Remove("Multi_password");
        /*Procedure to find whether the person logging in is a Administrator of that */
        /*To Check Whether user is Admin for the given Role*/

        if (strAdmin != "Active")
        {
            Response.Redirect("Procedure.aspx", false);
        }
        else
        {
            var OAccRequests = orfid.rp_implantAccountRequests(int.Parse(Session["AccID"].ToString()), null);
            foreach (var oreq in OAccRequests)
            {
                Response.Redirect("Procedure.aspx?QAlerts=Alerts");
            }
            Response.Redirect("Procedure.aspx", false);
        }
    }
    private bool RunningSubmit = false;
    protected void lnkbtn_Submit_Click(object sender, EventArgs e)
    {
        if (! RunningSubmit )
        {
            RunningSubmit = true;
            RfidAuthentication oauth = new RfidAuthentication();
            string[] MultiLoginAuthentication;
            char charAuth;
            string stringAdmin = "";
            if (ddl_loginhosp.SelectedValue != "")
            {
                MultiLoginAuthentication = oauth.MultiUserAuthentication(txt_Username.Text, Session["Multi_password"].ToString(), ddl_loginhosp.SelectedValue);
                charAuth = char.Parse(MultiLoginAuthentication[0].ToString());
                stringAdmin = MultiLoginAuthentication[1];
                Authenticated(stringAdmin);
            }
            else
            {
                Response.Redirect("Login.aspx?InvHosp=sel");
            }
            RunningSubmit = false ;
        }
    }
    protected void Button1_Click(object sender, EventArgs e) {
        Session["test1"] = "test1";
        object test1 = Session["test1"];
        Session["tess2"] = null;
        object test2 = Session["test2"];

        object test3 = Session["test3"];
        string ss = (string)test3;

    }
}
