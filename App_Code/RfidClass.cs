﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
/// <summary>
/// Summary description for RfidClass
/// </summary>
public class RfidClass
{
    RFIDDataContext Orfid = new RFIDDataContext();
    public void MailRequests(string MailTO, string MailFrom, string Subject, string Body)
    {
        MailMessage OMessageUser = new MailMessage(MailFrom, MailTO);
        SmtpClient osmtp = new SmtpClient();
        try
        {
            OMessageUser.Subject = Subject;
            OMessageUser.Body = Body;
            OMessageUser.IsBodyHtml = true;
            osmtp.Host = ConfigurationManager.AppSettings["EmailDomain"].ToString();
            osmtp.Send(OMessageUser);
        }
        catch (SmtpException ExSmtp)
        {
            if (ExSmtp.StatusCode == SmtpStatusCode.InsufficientStorage)
            {
                osmtp.Send(OMessageUser);
            }
        }
    }
    public System.Drawing.Color RFIDBackColor(int IntColor)
    {
       System.Drawing.Color [] RFIDColor = new[] {System.Drawing.Color.White,
                                                  System.Drawing.Color.LightGreen,
                                                  System.Drawing.Color.Red,
                                                  System.Drawing.Color.LightGray,
                                                  System.Drawing.Color.LightSlateGray,
                                                  System.Drawing.Color.LightSalmon,
                                                  System.Drawing.Color.LightYellow,
                                                  System.Drawing.Color.Khaki,
                                                  System.Drawing.Color.AntiqueWhite};

             return RFIDColor[IntColor];
    }
    public System.Drawing.Color RFIDForeColor(int IntColor)
    {
        System.Drawing.Color[] RFIDFColor = new[] {System.Drawing.Color.White,
                                                   System.Drawing.Color.DarkGreen,
                                                   System.Drawing.Color.Red,
                                                   System.Drawing.Color.Black,
                                                   System.Drawing.Color.Black,
                                                   System.Drawing.Color.DarkRed,
                                                   System.Drawing.Color.Orange,
                                                   System.Drawing.Color.Red,
                                                   System.Drawing.Color.Black};

            return RFIDFColor[IntColor];
    }
    public int CompareDay(string StrDayofWeek)
    {
        int IntDayOfWeek = 0;
        if (StrDayofWeek == "Monday")
            IntDayOfWeek = 0;
        if (StrDayofWeek == "Tuesday")
            IntDayOfWeek = -1;
        if (StrDayofWeek == "Wednesday")
            IntDayOfWeek = -2;
        if (StrDayofWeek == "Thursday")
            IntDayOfWeek = -3;
        if (StrDayofWeek == "Friday")
            IntDayOfWeek = -4;
        if (StrDayofWeek == "Saturday")
            IntDayOfWeek = -5;
        if (StrDayofWeek == "Sunday")
            IntDayOfWeek = -6;
        
        return IntDayOfWeek;
    }

    public string SupplierMailAddress(int IntSupplierID)
    {
       RFIDDataContext Orfid=new RFIDDataContext();
       string StrsupplierEmail = "";
               var Supp= from tblsupplier in Orfid.tblImplantSuppliers 
                  where tblsupplier.supId==IntSupplierID
                  select new{SupplierEmail=tblsupplier.supMailAddress};
        foreach (var osupplier in Supp)
        {
            StrsupplierEmail = osupplier.SupplierEmail;
        }
       return StrsupplierEmail  ;
    }
    /// <summary>
    /// We are using Supplier Email at the moment but finally we should be using Supplierwarehouse email address
    /// </summary>
    /// <param name="IntSupplierwhID"></param>
    /// <param name="IntDetailof"></param>
    /// <returns></returns>
    public string SupplierDetails(int IntSupplierwhID,int IntDetailof)
    {
        string StrsupplierEmail = "";
        string StrSupplierName = "";
        var Supp = Orfid.rp_implantGetSupplierDetails(IntSupplierwhID);
        foreach (var osupplier in Supp)
        {
            StrsupplierEmail = osupplier.SupplierEmail;
            StrSupplierName = osupplier.SupplierName;
        }
        if (IntDetailof == 1)
            return StrSupplierName;
        else
            return StrsupplierEmail;
    }

    public void BookingEmailSupplierwh(int IntSuppWhID,string StrFromAddress,string StrEmailBody, string StrEmailSubj)
    {
        string SupplierEmail = "";
        var SuppwhEmail = from suppAcc in Orfid.tblImplantAccounts
                          join suppRole in Orfid.tblImplantAccountRoles on
                          suppAcc.accId equals suppRole.arAccId
                          where suppRole.arSuppWhsId == IntSuppWhID && suppRole.arAdmin == 'Y'
                          select new { SupplierEmail = suppAcc.accUsername };
        foreach (var SEmail in SuppwhEmail)
        {
            SupplierEmail = SEmail.SupplierEmail;
        }
        MailRequests(SupplierEmail, StrFromAddress, StrEmailSubj, StrEmailBody);
    }
    //public int BatchID()
    //{
    //    SqlConnection Ocon = new SqlConnection();
    //    Ocon.ConnectionString = ConfigurationManager.AppSettings["RfidConnection"].ToString();
    //    SqlCommand Ocmd = new SqlCommand();
    //    Ocmd.CommandType = CommandType.StoredProcedure;
    //    Ocmd.CommandText = "rp_implantStartBatch";
        
    //    Ocmd.Parameters.Add("@orgID", SqlDbType.UniqueIdentifier) = "";
        
    //}

    public DataSet ProcedureDatasource(string strFromDate, string strToDate, int accId, string strorderno, string strHospID, string StrSuppwhID)
    {
        DataSet ODs = new DataSet();
        string StoredProcedure = "";
        string param = "";
       System.Nullable<int> intID;
        if (StrSuppwhID != "")
        {
            StoredProcedure = "Usp_implantOrdersBySupplierWhsWithStatus";
            intID = int.Parse(StrSuppwhID.ToString());
            param = "@supWhsId";
        }
        else
        {
            StoredProcedure = "Usp_implantOrdersByHospitalwithStatus";
            intID = int.Parse(strHospID.ToString());
            param = "@hospId";
        }   
            string Sqlcon = ConfigurationManager.AppSettings["RfidConnection"].ToString();
            SqlConnection ocon = new SqlConnection(Sqlcon);
            SqlCommand ocmd = new SqlCommand();
            ocmd.Connection = ocon;
            ocmd.CommandType = CommandType.StoredProcedure;
            ocmd.CommandText = StoredProcedure;
            ocon.Open();
            SqlDataAdapter oDA = new SqlDataAdapter();
            oDA.SelectCommand = ocmd;

            oDA.SelectCommand.Parameters.Add(param, SqlDbType.Int).Value = intID;
            if (strFromDate != null && strFromDate != "")
                oDA.SelectCommand.Parameters.Add("@fromDate", SqlDbType.DateTime).Value = DateTime.Parse(strFromDate);
            else
                oDA.SelectCommand.Parameters.Add("@fromDate", SqlDbType.DateTime).Value = null;
            if (strToDate != null && strToDate != "")
                oDA.SelectCommand.Parameters.Add("@toDate", SqlDbType.DateTime).Value = DateTime.Parse(strToDate);
            else
                oDA.SelectCommand.Parameters.Add("@toDate", SqlDbType.DateTime).Value = null;
            oDA.SelectCommand.Parameters.Add("@accId", SqlDbType.Int).Value = accId;    
            oDA.SelectCommand.Parameters.Add("@activeOnly", SqlDbType.Char).Value = 'N';
            oDA.SelectCommand.Parameters.Add("@orderNo", SqlDbType.VarChar, 50).Value = strorderno;
            //  DataSet ODs = new DataSet();
            oDA.Fill(ODs, "tbl_user");
            ocon.Close();            
            return ODs;
        }
    public int BookingRevisionNumber(int IntBookid)
    {
        int IntRevNumber = 0;
        var orevnumber = from tblrevbookings in Orfid.tblImplantBookingVersions
                         where tblrevbookings.revBookId==IntBookid
                         select new { RevisionNumber = tblrevbookings.revBookNo };
        foreach (var oRevisionnumber in orevnumber)
        {
            IntRevNumber = oRevisionnumber.RevisionNumber;
        }
        return IntRevNumber + 1;

    }
    public string GetOrderno(string gOrdID)
    {
        string strOrdno = "";
        Guid GorderID = new Guid(gOrdID);
        var ordno = from tblord in Orfid.tblImplantOrders
                    where tblord.ordId == GorderID
                    select new { OrderNo = tblord.ordNo };
        foreach (var vordno in ordno)
        {
            strOrdno = vordno.OrderNo;
        }
        return strOrdno;
    }

    #region PANEL COLOURS
    static public void PaintPanelsByStatus(Panel P, string S)
    {
        System.Drawing.Color LightGrey1 = System.Drawing.Color.FromArgb(231, 230, 229);
        System.Drawing.Color LightGrey2 = System.Drawing.Color.FromArgb(218, 216, 212);
        System.Drawing.Color Blue1 = System.Drawing.Color.FromArgb(130, 144, 222);
        System.Drawing.Color Blue2 = System.Drawing.Color.FromArgb(165, 191, 206);
        System.Drawing.Color Mocha = System.Drawing.Color.FromArgb(180, 171, 151);
        System.Drawing.Color Green1 = System.Drawing.Color.FromArgb(138, 184, 168);
        System.Drawing.Color Green2 = System.Drawing.Color.FromArgb(198, 204, 165);
        System.Drawing.Color Green0 = System.Drawing.Color.FromArgb(108, 104, 165);

        switch (S)
        {
            case "NewBooking": P.BackColor = LightGrey1; break;
            case "Acknowledged": P.BackColor = Green0; break;
            case "ShippingNoteReceived": P.BackColor = Green2; break;
            case "Received": P.BackColor = Green1; break;
            case "InTheatre": P.BackColor = Mocha; break;
            case "ReadyForDispatch": P.BackColor = Blue2; break;
            case "Dispatched": P.BackColor = Blue1; break;
            case "Complete": P.BackColor = LightGrey2; break;
            default: break;
        }
        return;
    }
    #endregion
    #region RECURSIVE FIND CONTROL
    static public Control FindControlRecursive(Control rootControl, string controlID)
    {
        if (rootControl.ID == controlID) return rootControl;

        foreach (Control controlToSearch in rootControl.Controls)
        {
            Control controlToReturn =
                FindControlRecursive(controlToSearch, controlID);
            if (controlToReturn != null) return controlToReturn;
        }
        return null;
    }
    #endregion

    #region SESSION VARIABLES
    public static int strInt(string v)
    {
        var HospIDstr = v;
        int HospIDint;
        var suc = int.TryParse(HospIDstr, out HospIDint);
        return HospIDint;
    }
    public static Nullable<int> strNInt(string v)
    {
        var HospIDstr = v;
        Nullable<int> HospIDint = null;
        int tempInt = -1;
        var suc = int.TryParse(HospIDstr, out tempInt);
        if (suc && tempInt != -1)
            HospIDint = tempInt;
        return HospIDint;
    }
    #endregion
}