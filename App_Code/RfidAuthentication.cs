﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for RfidAuthentication
/// </summary>
public class RfidAuthentication
{
    RFIDDataContext orfid = new RFIDDataContext();
    RfidClass orfidclass = new RfidClass();
    public string[] Authentication(string StrUsername, string StrPassword)
    {
        string[] AuthenArrary = new string[2];

        char CAuthenticated = 'N';
        string StrAccID = "";
        string StrAccSuppid = "";
        string StrAccHospID = "";
        string StrHospname = "";
        string StrHospCampus = "";
        string StrAccName = "";
        Guid gOrgiD;
        Guid greadID;

        string Sqlcon = ConfigurationManager.AppSettings["RfidConnection"].ToString();
        SqlConnection ocon = new SqlConnection(Sqlcon);
        SqlCommand ocmd = new SqlCommand();
        ocmd.Connection = ocon;
        ocmd.CommandType = CommandType.StoredProcedure;
        ocmd.CommandText = "rp_implantAccountVerify";
        ocon.Open();
        SqlDataAdapter oDA = new SqlDataAdapter();
        oDA.SelectCommand = ocmd;
        oDA.SelectCommand.Parameters.Add("@user", SqlDbType.VarChar, 200).Value = StrUsername;
        oDA.SelectCommand.Parameters.Add("@pass", SqlDbType.VarChar, 50).Value = StrPassword;
        DataSet ODs = new DataSet();
        oDA.Fill(ODs, "tbl_user");

        int IntRowcount = ODs.Tables[0].Rows.Count;
        if (IntRowcount == 1)
        {
            CAuthenticated = Convert.ToChar(ODs.Tables[0].Rows[0]["authenticated"].ToString());
            if (CAuthenticated == 'Y')
            {
                StrAccID = ODs.Tables[0].Rows[0]["accid"].ToString();
                StrAccSuppid = ODs.Tables[0].Rows[0]["accSuppid"].ToString();
                StrAccHospID = ODs.Tables[0].Rows[0]["accHospId"].ToString();
                StrHospname = ODs.Tables[0].Rows[0]["hospname"].ToString();
                StrHospCampus = ODs.Tables[0].Rows[0]["hospCampus"].ToString();
                StrAccName = ODs.Tables[0].Rows[0]["accName"].ToString();
                if (ODs.Tables[0].Rows[0]["orgid"] != null)
                {
                    if (ODs.Tables[0].Rows[0]["orgid"].ToString() != "")
                    {
                        gOrgiD = new Guid(ODs.Tables[0].Rows[0]["orgid"].ToString());
                        HttpContext.Current.Session["Se_ORGID"] = gOrgiD.ToString();
                    }
                }
                ocon.Close();

                HttpContext.Current.Session["User"] = StrAccName;
                HttpContext.Current.Session["AccID"] = StrAccID;

                if (StrAccHospID != "")//User belongs to Hospital and therefore Supplier details can be ignored
                {
                    /*Procedure to find whether the person logging in is a Administrator of that */
                    /*To Check Whether user is Admin for the given Role*/
                    var ORoleAdmin = orfid.rp_implantAccountHospitalRoles(int.Parse(StrAccID.ToString()), int.Parse(StrAccHospID.ToString()));

                    foreach (var oadmin in ORoleAdmin)
                    {
                        string StrAdmin = oadmin.arAdmin.ToString();
                        string Role = oadmin.roleId.ToString();
                        string Rolename = oadmin.roleName.ToString();
                        if (StrAdmin == "Y")
                            HttpContext.Current.Session["Admin"] = "Active";
                        /*Role 0 has access to all the roles and for consumption, if the item is dispatched then all the scain functionality dissappers*/
                        if (int.Parse(Role) == 1 || int.Parse(Role) == 0)
                        {
                            HttpContext.Current.Session["OrderLoanKit"] = "Active";
                        }
                        if (int.Parse(Role) == 2 || int.Parse(Role) == 0)
                        {
                            HttpContext.Current.Session["ProcessLoankit"] = "Active";
                        }
                        if (int.Parse(Role) == 3 || int.Parse(Role) == 0)
                        {
                            HttpContext.Current.Session["FinancialRecording"] = "Active";
                        }
                    }
                    HttpContext.Current.Session["HospID"] = StrAccHospID;
                    if (StrHospname != "")
                        HttpContext.Current.Session["Hospname"] = StrHospname;
                    if (StrHospCampus != "")
                    HttpContext.Current.Session["HospCampus"] = StrHospCampus;
                }
                else
                {
                    if (StrAccSuppid != "")//Verify Supplierdetails function in rfidclass 
                    {
                        var rolesuppadmin = orfid.rp_implantAccountSupplierWhsRoles(int.Parse(StrAccID.ToString()), int.Parse(StrAccSuppid.ToString()));
                        foreach (var osuppadmin in rolesuppadmin)
                        {
                            string StrAdmin = osuppadmin.arAdmin.ToString();
                            if (StrAdmin == "Y")
                                HttpContext.Current.Session["Admin"] = "Active";
                        }
                        HttpContext.Current.Session["SuppName"] = orfidclass.SupplierDetails(int.Parse(StrAccSuppid), 1);
                        HttpContext.Current.Session["SuplierID"] = StrAccSuppid;// It is SupplierWarehouseID
                    }
                }
            }
            AuthenArrary[0] = CAuthenticated.ToString();
            if (HttpContext.Current.Session["Admin"] != null)
            {
                AuthenArrary[1] = HttpContext.Current.Session["Admin"].ToString();
            }
            else
            {
                AuthenArrary[1] = null;
            }
            return AuthenArrary;
        }
        else
        {
            CAuthenticated = 'M';
            AuthenArrary[0] = CAuthenticated.ToString();
            AuthenArrary[1] = null;
            StrAccID = ODs.Tables[0].Rows[0]["accid"].ToString();
            HttpContext.Current.Session["AccID"] = StrAccID;
            return AuthenArrary;
        }
    }
    public string[] MultiUserAuthentication(string StrUsername, string StrPassword, string StrMultiHospID)
    {
        string[] AuthenticatedArrary = new string[2];

        char CAuthenticated = 'N';
        string StrAccID = "";
        string StrAccSuppid = "";
        string StrAccHospID = "";
        string StrHospname = "";
        string StrHospCampus = "";
        string StrAccName = "";
        Guid gOrgiD;
        Guid greadID;

        string Sqlcon = ConfigurationManager.AppSettings["RfidConnection"].ToString();
        SqlConnection ocon = new SqlConnection(Sqlcon);
        SqlCommand ocmd = new SqlCommand();
        ocmd.Connection = ocon;
        ocmd.CommandType = CommandType.StoredProcedure;
        ocmd.CommandText = "rp_implantAccountVerify";
        ocon.Open();
        SqlDataAdapter oDAp = new SqlDataAdapter();
        oDAp.SelectCommand = ocmd;
        oDAp.SelectCommand.Parameters.Add("@user", SqlDbType.VarChar, 200).Value = StrUsername;
        oDAp.SelectCommand.Parameters.Add("@pass", SqlDbType.VarChar, 50).Value = StrPassword;
        DataSet ODsp = new DataSet();
        oDAp.Fill(ODsp, "tbl_Multiuser");
        //New Code Added to accomodate multiple hospitals
        //if more than one rows passin a session HospId and we can compare hospId with row and get other information
        if (StrMultiHospID != null && StrMultiHospID != "")
        {
            for (int i = 0; i < ODsp.Tables["tbl_Multiuser"].Rows.Count; i++)
            {
                StrAccHospID = ODsp.Tables["tbl_Multiuser"].Rows[i]["accHospId"].ToString();
                if (StrAccHospID == StrMultiHospID)
                {
                    CAuthenticated = Convert.ToChar(ODsp.Tables["tbl_Multiuser"].Rows[i]["authenticated"].ToString());
                    if (CAuthenticated == 'Y')
                    {
                        StrAccID = ODsp.Tables["tbl_Multiuser"].Rows[i]["accid"].ToString();
                        StrAccSuppid = ODsp.Tables["tbl_Multiuser"].Rows[i]["accSuppid"].ToString();
                        StrAccHospID = ODsp.Tables["tbl_Multiuser"].Rows[i]["accHospId"].ToString();
                        StrHospname = ODsp.Tables["tbl_Multiuser"].Rows[i]["hospname"].ToString();
                        StrHospCampus = ODsp.Tables["tbl_Multiuser"].Rows[i]["hospCampus"].ToString();
                        StrAccName = ODsp.Tables["tbl_Multiuser"].Rows[i]["accName"].ToString();
                        if (ODsp.Tables["tbl_Multiuser"].Rows[i]["orgid"] != null)
                        {
                            if (ODsp.Tables["tbl_Multiuser"].Rows[i]["orgid"].ToString() != "")
                            {
                                gOrgiD = new Guid(ODsp.Tables["tbl_Multiuser"].Rows[i]["orgid"].ToString());
                                HttpContext.Current.Session["Se_ORGID"] = gOrgiD.ToString();
                            }
                        }
                        //if (ODsp.Tables["tbl_Multiuser"].Rows[i]["readerid"] != null)
                        //{
                        //    if (ODsp.Tables["tbl_Multiuser"].Rows[i]["readerid"].ToString() != "")
                        //    {
                        //        greadID = new Guid(ODsp.Tables["tbl_Multiuser"].Rows[i]["readerid"].ToString());
                        //        //HttpContext.Current.Session["ReaderID"] = greadID.ToString();
                        //    }
                        //}
                        ocon.Close();

                        HttpContext.Current.Session["User"] = StrAccName;
                        HttpContext.Current.Session["AccID"] = StrAccID;

                        if (StrAccHospID != "")//User belongs to Hospital and therefore Supplier details can be ignored
                        {
                            /*Procedure to find whether the person logging in is a Administrator of that */
                            /*To Check Whether user is Admin for the given Role*/
                            var ORoleAdmin = orfid.rp_implantAccountHospitalRoles(int.Parse(StrAccID.ToString()), int.Parse(StrAccHospID.ToString()));

                            foreach (var oadmin in ORoleAdmin)
                            {
                                string StrAdmin = oadmin.arAdmin.ToString();
                                string Role = oadmin.roleId.ToString();
                                string Rolename = oadmin.roleName.ToString();
                                if (StrAdmin == "Y")
                                    HttpContext.Current.Session["Admin"] = "Active";
                                /*Role 0 has access to all the roles and for consumption, if the item is dispatched then all the scain functionality dissappers*/
                                if (int.Parse(Role) == 1 || int.Parse(Role) == 0)
                                {
                                    HttpContext.Current.Session["OrderLoanKit"] = "Active";
                                }
                                if (int.Parse(Role) == 2 || int.Parse(Role) == 0)
                                {
                                    HttpContext.Current.Session["ProcessLoankit"] = "Active";
                                }
                                if (int.Parse(Role) == 3 || int.Parse(Role) == 0)
                                {
                                    HttpContext.Current.Session["FinancialRecording"] = "Active";
                                }
                            }

                            HttpContext.Current.Session["HospID"] = StrAccHospID;
                            if (StrHospname != "")
                                HttpContext.Current.Session["Hospname"] = StrHospname;
                           if(StrHospCampus!="")
                            HttpContext.Current.Session["HospCampus"] = StrHospCampus;
                        }
                        else
                        {
                            if (StrAccSuppid != "")//Verify Supplierdetails function in rfidclass 
                            {
                                var rolesuppadmin = orfid.rp_implantAccountSupplierWhsRoles(int.Parse(StrAccID.ToString()), int.Parse(StrAccSuppid.ToString()));
                                foreach (var osuppadmin in rolesuppadmin)
                                {
                                    string StrAdmin = osuppadmin.arAdmin.ToString();
                                    if (StrAdmin == "Y")
                                        HttpContext.Current.Session["Admin"] = "Active";
                                }
                                HttpContext.Current.Session["SuppName"] = orfidclass.SupplierDetails(int.Parse(StrAccSuppid), 1);
                                HttpContext.Current.Session["SuplierID"] = StrAccSuppid;// It is SupplierWarehouseID
                            }
                        }
                    }
                }
            }
        }
        AuthenticatedArrary[0] = CAuthenticated.ToString();
        if (HttpContext.Current.Session["Admin"] != null)
        {
            AuthenticatedArrary[1] = HttpContext.Current.Session["Admin"].ToString();
        }
        else
        {
            AuthenticatedArrary[1] = null;
        }
        return AuthenticatedArrary;
    }
}
