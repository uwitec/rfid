﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<link href="StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <link href="css/template.css" rel="stylesheet" type="text/css" />
    <link href="css/rounded.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>    <link href="css/ie6.css" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if IE 7]>    <link href="css/ie7.css" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if IE 8]>    <link href="css/ie8.css" rel="stylesheet" type="text/css" /><![endif]-->
<!--[if IE 9]>    <link href="css/ie9.css" rel="stylesheet" type="text/css" /><![endif]-->
</head>
<script type="text/javascript">
    function BarcodeLogin(src) {
        var txt = src.value;
        if (txt.substring(0, 1) == '~') {
            __doPostBack('lnkbtn_login', '');
        }
    }
    function disableCtrlKeyCombination(e) {
        //list all CTRL + key combinations you want to disable
        var forbiddenKeys = new Array('p', 'a', 'n', 'j', 't');
        var key, isCtrl;
        if (window.event) {
            key = window.event.keyCode;         //IE
            isCtrl = e.event.ctrlKey;
        } else {
            key = e.which;                      //firefox and chrome
            isCtrl = e.ctrlKey;
        }
        if (isCtrl) {
            for (i = 0; i < forbiddenKeys.length; i++) {
                if (forbiddenKeys[i].toLowerCase() == String.fromCharCode(key).toLowerCase()) {
                    try {
                        if (window.event)
                            window.event.keyCode = 0;   //Needed to stop print popup on ^p
                        else
                            e.which = 0;
                    } catch (e) { }
                    return false;
                }
            }
        }
        return true;
    }
</script>
<body onkeypress="return disableCtrlKeyCombination(event);" onkeydown="return disableCtrlKeyCombination(event);" onkeyup="return disableCtrlKeyCombination(event);">
    <form id="form1" defaultbutton="lnkbtn_login" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"/>
<div id="content-box">
	<div id="loginbox" class="padding" runat="server">
		<div class="clr"></div>
		<div id="element-box" class="login">
            <div id="div_msgsuccess" runat="server">
                <div id="message" class="success">
                    <div class="message-list" id="toolbar">
                        <ul>
                            <li class="button" id="toolbar-new"><span class="icon-32-publish"></span></li>
                        </ul>
                    </div>
                    <p><asp:Label ID="lbl_banner" runat="server"/></p>
                </div>
            </div>
            <div id="div_msgerror" runat="server">
                <div id="message1" class="error1">
                    <div class="message-list1" id="Div1">
                        <ul>
                            <li class="button" id="Li1"><span class="icon-32-delete"></span></li>
                        </ul>
                    </div>
                    <p><asp:Label ID="lbl_verify" runat="server"/></p>
                </div>
            </div>
            <div id="border-top" class="login">
                <div>
                    <div >
                        <h1>Login</h1>
                    </div>
                </div>
            </div>
<div class="m">
    <div id="section-box">
        <div>
            <fieldset class="loginform">
                <table>
                    <tr>
                        <td>UserName:</td>
                        <td><asp:TextBox ID="txt_Username" runat="server" CssClass="logintextbox" /></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><asp:TextBox ID="Txt_Password" runat="server" TextMode="Password" CssClass="logintextbox"/></td>
                    </tr>
                </table>
                <div class="button-holder">
                    <asp:linkbutton ID="lnkbtn_login" runat="server" onclick="lnkbtn_login_Click" class="button-dynamic"><div id="L"></div><div id="C">&nbsp; LOGIN &nbsp;</div><div id="R"></div></asp:linkbutton>
                    <p><asp:LinkButton ID="lnk_newuser" runat="server" onclick="lnk_newuser_Click" >New User Sign Up</asp:LinkButton></p>
                    <p><asp:LinkButton ID="lnk_forgotpwd" runat="server" onclick="lnk_forgotpwd_Click">Forgot password?</asp:LinkButton></p>
                </div>				
            </fieldset>
	        <%--<div class="clr"></div>--%>
        </div>
    </div>
    <div class="clr"></div>
</div>

<%--FOOTER--%>
            <div id="border-bottom" class="login">
                <div>
                    <div id="logo">
                        <img src="images/data-agility-logo.jpg" alt="Data Agility" width="194px" height="36px" />
                    </div>
                    <div>
                        <div id="footer">
                            <p class="copyright">Copyright Data Agility 2012 | Email the <a href="mailto:rod_Perry@dataagility.com">Administrator</a></p>
                        </div>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
	    </div>
    </div>
    
<%--This is Additional Functionality for Popup to select Hospital--%>	  
    <asp:Panel ID="Panel_multiHosp" runat="server" Width="300px"  >
        <div>
            <fieldset>
                <legend>Select Hospital</legend>  
                <table>
                    <tr>
                        <td>Hospital:</td>
                        <td>
                            <asp:DropDownList ID="ddl_loginhosp" runat="server" AppendDataBoundItems="True" Height="16px" Width="190px">
                                <asp:ListItem></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <div class=" width-100px height-1px FloatLeft"></div>
                <asp:linkbutton ID="lnkbtn_Submit" runat="server" onclick="lnkbtn_Submit_Click" class="button-dynamic FloatLeft" ><div id="L"></div><div id="C">&nbsp; SUBMIT &nbsp;</div><div id="R"></div></asp:linkbutton>
                <div class="clr"></div>
            </fieldset>
        </div>
    </asp:Panel>
    <asp:Label ID="label_test" runat="server" Text="" ></asp:Label>
    <asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalloginBackground" PopupControlID="Panel_multiHosp" TargetControlID="label_test" EnableViewState="false" />    
</div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
    </form>
</body>
</html>
