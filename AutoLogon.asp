<%
Response.Expires = -10000

errMsg = ""
MonthNames = "JanFebMarAprMayJunJulAugSepOctNovDec"
connStr="Provider=SQLOLEDB.1;Password=rfiduser;Persist Security Info=True;User ID=RfidUser;Initial Catalog=RFIDProd;Data Source=SQL08"
on error resume next
set Conn = Server.CreateObject("ADODB.Connection")
if err=0 then
    Conn.Open connStr
end if
if err<>0 then
    errMsg = "Error " & cstr(err.description)
end if

Function BlankIfNull(inText)
  if IsNull(inText) then
    BlankIfNull = "&nbsp;"
  elseif inText="" then
    BlankIfNull = "&nbsp;"
  else
    BlankIfNull = inText
  end if
End Function

Function NullOrValue(inText)
  if inText = "" then
    NullOrValue = "NULL"
  else
    NullOrValue = "'" & Replace(inText,"'","''") & "'"
  end if
End Function

Function NullOrY(chkValue)
  if chkValue = "" then
    NullOrY = "NULL"
  else
    NullOrY = "'Y'"
  end if
End Function

Function FormatDate(inDate)
  dim result
  result = DatePart("d",inDate) & "-"
  mth = DatePart("m",inDate)
  result = result & mid(MonthNames,3*mth-2,3) & "-" & DatePart("yyyy",inDate)
  FormatDate = result
End Function

%>

<html>
<head>
<title>eHealthFlow Auto Logon</title>
<style type="text/css">
  .label {color:#3366CC;font-weight:bold;}
  .head {color:darkblue;background-color:lightgrey;font-weight:bold;}
</style>
</head>
<body>
<div style="position:absolute; overflow:hidden; left:0px; top:0px; z-index:1; width:1024px; height:100px;">
  <img src="images/header-bg.jpg" width="1024" height="100">
</div>
<div style="position:absolute; overflow:hidden; left:40px; top:37px; width:194px; height:36px; z-index:10;">
  <img src="images/data-agility-logo.jpg" width="194" height="36">
</div>
<div style="position:absolute; overflow:hidden; left:300px; top:32px; width:700px; height:44px; z-index:10;">
  <font style="font-size:36px;"><b>Loan Kit Booking Acknowledgment</font>
</div>
<div style="position:absolute; overflow:hidden; left:20px; top:120px; width:810px; height:460px; z-index:10;">
<%
  if errMsg<>"" then
    Response.Write "<h1>" & errMsg & "</h1>" & Chr(10)
  elseif Request.Form("Acknowledge")="" then
    sql = "exec rp_implantHtmlLinkGetBooking @key='" & Request.QueryString("key") & "'"
    set RS = Conn.Execute(sql)
    Response.Write "<center><table border=""0"" cellspacing=""4"" cellpadding=""2"">" & Chr(10)
    if not RS.EOF then
      Response.Write "<tr><td colspan=""4"" align=""center"" class=""head"">Welcome " & RS("RepName") & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Revision Number:</td><td>" & Cstr(RS("revBookNo")) & "</td>"
      Response.Write "<td class=""label"">BookingID:</td><td>" & Cstr(RS("bookId")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Procedure:</td><td>" & BlankIfNull(RS("revProcedure")) & "</td>"
      Response.Write "<td class=""label"">Contact Number:</td><td>" & BlankIfNull(RS("revContactNo")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Patient UR:</td><td>" & BlankIfNull(RS("ptUR")) & "</td>"
      Response.Write "<td class=""label"">Supplier:</td><td>" & BlankIfNull(RS("supName")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Patient:</td><td>" & BlankIfNull(RS("ptFamilyName")) 
      if not IsNull(RS("Gender")) then Response.Write " (" & RS("Gender") & ")"
      Response.Write "</td>"
      Response.Write "<td class=""label"">Surgeon:</td><td>" & BlankIfNull(RS("Surgeon")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Operation Date:</td><td>" & FormatDate(RS("revOperationDttm")) & "</td>"
      Response.Write "<td class=""label"">Patient Type:</td><td>" & BlankIfNull(RS("PatientType")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Operation Time:</td><td>&nbsp;</td>"
      Response.Write "<td class=""label"">Products:</td><td>" & BlankIfNull(RS("revProductsReqd")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Operation Type:</td><td>" & BlankIfNull(RS("OpType")) & "</td>"
      Response.Write "<td class=""label"">Side:</td><td>" & BlankIfNull(RS("KitSide")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Case Type:</td><td>" & BlankIfNull(RS("CaseType")) & "</td>"
      Response.Write "<td class=""label"">Kit Type:</td><td>" & BlankIfNull(RS("KitType")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td class=""label"">Notes:</td><td colspan=""3"">" & BlankIfNull(RS("revNotes")) & "</td></tr>" & Chr(10)
      Response.Write "<tr><td calspan=""4"">&nbsp;</td></tr>" & Chr(10)
      if IsNull(RS("AcknowledgeText")) then
        Response.Write "<form name=""frmMain"" method=POST action=""AutoLogon.asp?key=" & Request.QueryString("key") & """>" & Chr(10) 
        Response.Write "<tr><td class=""label"">Comment:</td><td colspan=""3""><input type=""text"" name=""comment"" size=""60""></td></tr>" & Chr(10)
        Response.Write "<tr><td class=""label"">Rep Attendance Reqd:</td><td><input type=""checkbox"" name=""chkRep""></td>"
	Response.Write "<td colspan=""2"" align=""center""><input type=""submit"" name=""Acknowledge"" value=""Acknowledge"">"
        Response.Write "<input type=""hidden"" name=""accId"" value=""" & Cstr(RS("htmlAccId")) & """></td></tr>" & Chr(10)
        Response.Write "</form>" & Chr(10)
      else
        Response.Write "<tr><td align=""center"" colspan=""4"">" & RS("AcknowledgeText") & "</td></tr>" & Chr(10)
      end if
      RS.Close
    end if
    Response.Write "</table></center>" & Chr(10)
    Conn.Close
  else
    sql = "exec rp_implantHtmlLinkBookingAcknowledge @key='" & Request.QueryString("key") & "',@comment=" & NullOrValue(Request.Form("comment"))
    sql = sql & ",@repReqd=" & NullOrY(Request.Form("chkRep"))
    Conn.Execute(sql)
    Response.Write "<h1>The booking has now been acknowledged!</h1>" & Chr(10)
  end if
%>
</div>
<div style="position:absolute; overflow:hidden; left:20px; top:580px; width:1000px; height:40px; z-index:10;">
Please contact the <a href="mailto:rod_Perry@dataagility.com">Administrator</a> should you need assistance.
</div>

</body>
</html>
