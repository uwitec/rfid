﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Diagnostics;
public partial class Scan : System.Web.UI.Page
{
    #region DEFENITIONS
    RFIDDataContext Orfid = new RFIDDataContext();
    RfidClass ORfidclass = new RfidClass();

    protected override void Render(HtmlTextWriter writer)
    {
        for (int grdrows = 0; grdrows <= gv_Scan.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(gv_Scan.UniqueID, "Select$" + grdrows.ToString());
        }
        //for (int grdrow = 0; grdrow <= GridView_Readers.Rows.Count; grdrow++)
        //{
        //    ClientScript.RegisterForEventValidation(GridView_Readers.UniqueID, "Select$" + grdrow.ToString());
        //}
        for (int grdrows = 0; grdrows <= GridView_orderDetails.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(GridView_orderDetails.UniqueID, "Select$" + grdrows.ToString());
        }
        for (int grdrows = 0; grdrows <= GridView_consumption.Rows.Count; grdrows++)
        {
            ClientScript.RegisterForEventValidation(GridView_consumption.UniqueID, "Select$" + grdrows.ToString());
        }
        base.Render(writer);
    }
    #endregion
    #region PAGE PROPERTIES
    public int HOSPID { get { return RfidClass.strInt(Session["HospID"].ToString()); } }
    public int ACCID { get { return RfidClass.strInt(Session["accID"].ToString()); } }
    /// <summary>
    /// The Order ID for the current booking.
    /// </summary>
    public Guid ORDID { get { return new Guid(Request.QueryString["ordID"].ToString()); } }
    public bool PROCESSLOANKIT
    {
        get
        {
            if (Session["ProcessLoankit"] == null)
                return false;
            else
                return true;
        }
    }
    public bool FINCANCIALRECORDING
    {
        get
        {
            if (Session["FinancialRecording"] == null)
                return false;
            else
                return true;
        }
    }
    public bool SHIPPED
    {
        get
        {
            if (lbl_Status.Text == "Complete" || lbl_Status.Text == "Dispatched")
                return true;
            else
                return false;
        }
    }
    /// <summary>
    /// Holds and creates the Batch ID when adding new Items or scanning in chunks of items
    /// </summary>
    private int IntBatchID
    {
        get
        {
            string StrbatchID = "-1";
            if (Session["StrbatchID"] != null)
            {
                if (Session["StrbatchID"].ToString() != "")
                {
                    StrbatchID = Session["StrbatchID"].ToString();
                }
            }
            if (StrbatchID == "-1")
            {
                try
                {
                    Guid GOrgID = new Guid(Session["Se_ORGID"].ToString());
                    System.Nullable<int> intbatchID = null;
                    Orfid.rp_implantStartBatch(GOrgID, Session["User"].ToString(), ref intbatchID);
                    if (intbatchID.HasValue)
                    {
                        StrbatchID = intbatchID.ToString();
                        IntBatchID = int.Parse(StrbatchID);
                    }
                }
                catch (Exception ExpDetailsWindow)
                {
                    Orfid.Usp_WebErrors("DetailsWindow.aspx", "Page Load", DateTime.Now.AddDays(0), ExpDetailsWindow.Message.ToString());
                    return int.Parse(StrbatchID.ToString());
                }
            }
            return int.Parse(StrbatchID.ToString());
        }
        set { Session["StrbatchID"] = value.ToString(); }
    }
    #endregion

    #region PAGE LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.ServerVariables["http_referer"] != null)
        {
            try
            {
                if (Page.IsPostBack == false)
                {
                    //Guid gOrderID = new Guid(Session["SuppOrderID"].ToString()); 
                    CleanButtonsAndFields();
                    Timer_gridscan.Enabled = false;//By Default Timer is disabled

                    if (ORDID != null)
                    {
                        //Page.ClientScript.RegisterStartupScript(this.GetType(),"CreateGridHeader", "<script>CreateGridHeader('divmt',gv_Scan','divhead');</script>",true);
                        //Panel_Addnewitem.Visible = false;
                        //Panel_viewitem.Visible = false;
                        //Panel_multiHosp.Visible = false;
                        //Panel_readerstate.Visible = false;
                        SupplierAccess();//Supplieraccess to be tested.
                        Grid_ScanIN();
                        RolebasedAccesstoScan();
                        if (Session["ReaderName"] != null)
                            lbl_Reader.Text = Session["ReaderName"].ToString();
                    }
                }
            }
            catch (Exception EScanload)
            {
                Orfid.Usp_WebErrors("scan.aspx", "Page Load", DateTime.Now.AddDays(0), EScanload.Message.ToString());
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }
    #region ROLES
    private void RolebasedAccesstoScan()
    {
        if (PROCESSLOANKIT)//This Role is must for a Person to Scan and Dispatch Items
            ScanFunctionalityForRole_Toggle(true);
        //STP_Consumption_Vis = false;
        else
            ScanFunctionalityForRole_Toggle(false);
        if (FINCANCIALRECORDING)
            ConfirmConsumption();
        else
            STP_Consumption_Vis = false; //= STP_Print_Vis 
    }
    private void ScanFunctionalityForRole_Toggle(bool v)
    {
        if (SHIPPED)
            v = false;
        //Scan Toolbar Panel
        STP_ScanIn_Vis = v;
        STP_Dispatch_Vis = v;
        STP_Usage_Vis = v;
        STP_Ship_Vis = v;
        STP_AddNewItem_Vis = v;
        lnkbtn_VPD_Save.Enabled = v;
    }
    private void SupplierAccess()
    {
        /*Code is to disable all functionality if the user has a Supplier role*/
        if (Session["SuppName"] != null)
        {
            if (Session["SuppName"] != "")
            {
                ScanFunctionalityForRole_Toggle(false);
                //ModalPopupExtender_Consumption.Enabled = false;

                var tblOrd = from tborders in Orfid.tblImplantOrders
                             where tborders.ordId == ORDID
                             select new { OrgID = tborders.ordOrgId };
                foreach (var Vorgid in tblOrd)
                {
                    Session["Se_ORGID"] = Vorgid.OrgID.ToString();
                }
            }
        }
    }
    #endregion
    #endregion

    #region SCAN TOOLBAR PANEL
    public void CleanButtonsAndFields()
    {
        //Hide runtime buttons
        STP_Save_Vis = false;
        STP_Undo_Vis = false;

        //Set all active buttons to inactive
        STP_ImgScanIN_Active_Toggle(false);
        STP_ImgDispatch_Active_Toggle(false);
        STP_ImgUsage_Active_Toggle(false);
    }

    #region SCAN IN ITEMS
    public bool STP_ScanIn_Vis
    {
        get
        {
            return lnkbtn_STP_ScanIn.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_STP_ScanIn.Visible = true;
            else
                lnkbtn_STP_ScanIn.Visible = false;
        }
    }
    public void STP_ImgScanIN_Active_Toggle(bool v)
    {
        spanactive.Visible = v;
        Spaninactive.Visible = !v;
    }

    protected void lnkbtn_STP_ScanIn_Click(object sender, EventArgs e)
    {
        try
        {
            // lnkbtn_close.Visible = true;
            STP_ImgScanIN_Active_Toggle(true);
            STP_ImgDispatch_Active_Toggle(false);
            STP_ImgUsage_Active_Toggle(false);
            if (Session["ReceiveType"] != null)
                Session.Remove("ReceiveType");
            Session["ReceiveType"] = "2";
            CommandReader();
            Functionreadnewtags("0");
            RolebasedAccesstoScan();
            pnl_ScannerFeedback.Visible = false; //Will become visible on each tick
        }
        catch (Exception EImgbtn_ScanIN)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "Imgbtn_Scanin_Click", DateTime.Now.AddDays(0), EImgbtn_ScanIN.Message.ToString());
        }
    }
    private void Functionreadnewtags(string StrReadnewTags)
    {
        if (int.Parse(StrReadnewTags.ToString()) != 0)
        {
            STP_Undo_Vis = true;
            STP_Save_Vis = true;
        }
        else
        {
            STP_Undo_Vis = false;
            STP_Save_Vis = false;
        }
    }
    #endregion
    #region USAGE OF ITEMS
    public bool STP_Usage_Vis
    {
        get
        {
            return lnkbtn_STP_Usage.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_STP_Usage.Visible = true;
            else
                lnkbtn_STP_Usage.Visible = false;
        }
    }
    public void STP_ImgUsage_Active_Toggle(bool v)
    {
        spanusageactive.Visible = v;
        spanusageinactive.Visible = !v;
    }

    protected void lnkbtn_STP_Usage_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReceiveType"] != null)
                Session.Remove("ReceiveType");
            Session["ReceiveType"] = "3";
            STP_ImgUsage_Active_Toggle(true);
            STP_ImgDispatch_Active_Toggle(false);
            STP_ImgScanIN_Active_Toggle(false);
            Functionreadnewtags("0");
            CommandReader();
            RolebasedAccesstoScan();
        }
        catch (Exception exlnkUsage)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnk_usage_Click", DateTime.Now.AddDays(0), exlnkUsage.Message.ToString());
        }
    }
    #endregion
    #region DISPATCH ITEMS
    public bool STP_Dispatch_Vis
    {
        get
        {
            return lnkbtn_STP_Dispatch.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_STP_Dispatch.Visible = true;
            else
                lnkbtn_STP_Dispatch.Visible = false;
        }
    }
    public void STP_ImgDispatch_Active_Toggle(bool v)
    {
        SpandisActive.Visible = v;
        Spandisinactive.Visible = !v;
    }

    protected void lnkbtn_STP_Dispatch_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ReceiveType"] != null)
                Session.Remove("ReceiveType");
            Session["ReceiveType"] = "4";
            CommandReader();
            Functionreadnewtags("0");
            STP_ImgDispatch_Active_Toggle(true);
            STP_ImgScanIN_Active_Toggle(false);
            STP_ImgUsage_Active_Toggle(false);
            RolebasedAccesstoScan();
        }
        catch (Exception EDispatch)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_Dispatch_Click", DateTime.Now.AddDays(0), EDispatch.Message.ToString());
        }
    }

    ///// <summary>
    ///// Dispatch will add Dispatchdate into tblImplantOrder 
    ///// </summary>
    ///// <param name="sender"></param>
    ///// <param name="e"></param>
    //protected void Imgbtn_Dispatch_Click(object sender, ImageClickEventArgs e)
    //{
    //    try
    //    {
    //        if (Session["ReceiveType"] != null)
    //            Session.Remove("ReceiveType");
    //        Session["ReceiveType"] = "4";
    //        CommandReader();
    //    }
    //    catch (Exception EDispatch)
    //    {
    //        Orfid.Usp_WebErrors("Scan.aspx", "Imgbtn_Dispatch_Click", DateTime.Now.AddDays(0), EDispatch.Message.ToString());
    //    }
    //}
    #endregion
    #region RECONCILE ITEMS
    //visible for all roles
    protected void lnkbtn_STP_Reconcile_Click(object sender, EventArgs e)
    {
        try
        {
            string attachment = "attachment; filename=" + ORfidclass.GetOrderno(ORDID.ToString()).ToString() + ".xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GridView_orderDetails.RenderControl(htw);
            gv_Scan.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        catch (System.Threading.ThreadAbortException lException)
        {
            // do nothing
        }
        catch (Exception ExReconcile)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_reconcile_Click", DateTime.Now.AddDays(0), ExReconcile.Message.ToString());
        }
    }
    #endregion
    #region SHIPMENT OF ITEMS
    public bool STP_Ship_Vis
    {
        get
        {
            return lnkbtn_STP_Shipped.Visible;
        }
        set
        {
            // Shipment can only be confirmed if the # of items in the tblimplantDispatch for that particular OrderID is more than 1(ie)Shipment can only be done after Dispatch 
            int Intshipconfirm = (from tblOrderdispatch in Orfid.tblImplantOrderDispatches
                                  where tblOrderdispatch.disOrderId == ORDID
                                  select tblOrderdispatch).Count();
            if (Intshipconfirm > 0 && value && ItemsUnShipped())
                lnkbtn_STP_Shipped.Visible = true;
            else
                lnkbtn_STP_Shipped.Visible = false;
        }
    }

    protected void lnkbtn_STP_Shipped_Click(object sender, EventArgs e)
    {
        try
        {
            RFID orfidcs = new RFID();
            orfidcs.StopReader();
            var ShipOrder = (from tblOrders in Orfid.tblImplantOrders
                             where tblOrders.ordId == ORDID
                             select tblOrders).Single();
            ShipOrder.ordCreateDispatch = DateTime.Now.AddDays(0);
            ShipOrder.ordDispatchId = int.Parse(Session["AccID"].ToString());
            Orfid.SubmitChanges();
            OrderDetails_Populate();
            RolebasedAccesstoScan();
        }
        catch (Exception ExlnkShipped)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_STP_Shipped_Click", DateTime.Now.AddDays(0), ExlnkShipped.Message.ToString());
        }
    }
    #endregion
    #region SAVE BOOKING
    public bool STP_Save_Vis
    {
        get
        {
            return lnkbtn_STP_Save.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_STP_Save.Visible = true;
            else
                lnkbtn_STP_Save.Visible = false;
        }
    }

    protected void lnkbtn_STP_Save_Click(object sender, EventArgs e)
    {
        try
        {
            Session["ReceiveType"] = "0";
            CommandReader();
            Timer_gridscan.Enabled = false;
            Response.Redirect("Procedure.aspx");
        }
        catch (Exception Exsave)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_STP_Save_Click", DateTime.Now.AddDays(0), Exsave.Message.ToString());
        }
    }
    #endregion
    #region UNDO SCANNING
    public bool STP_Undo_Vis
    {
        get
        {
            return lnkbtn_STP_Undo.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_STP_Undo.Visible = true;
            else
                lnkbtn_STP_Undo.Visible = false;
        }
    }

    protected void lnkbtn_STP_Undo_Click(object sender, EventArgs e)
    {
        try
        {
            int IntScanBatchID = 0;
            if (Session["IntScanBatchID"] != null)
            {
                if (Session["IntScanBatchID"].ToString() != "")
                {
                    IntScanBatchID = int.Parse(Session["IntScanBatchID"].ToString());
                }
            }
            Guid gOrgID = new Guid(Session["Se_ORGID"].ToString());
            Guid greadID = new Guid(Session["ReaderID"].ToString());
            Orfid.rp_implantRollback(gOrgID, ORfidclass.GetOrderno(ORDID.ToString()).ToString(), IntScanBatchID);
            var tblreader = Orfid.tblImplantReaders.Single(tblread => tblread.readId == greadID);
            tblreader.readNewTags = 0;
            Orfid.SubmitChanges();
            Functionreadnewtags("0");
            Grid_ScanIN();
        }
        catch (Exception EImgbtn_undo)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "Imgbtn_undo_Click", DateTime.Now.AddDays(0), EImgbtn_undo.Message.ToString());
        }
    }
    #endregion
    #region CONSUMPTION OF ITEMS
    public bool STP_Consumption_Vis
    {
        get
        {
            if (lnkbtn_STP_Consumption.CssClass == "DisplayNone")
                return false;
            else
                return true;
        }
        set
        {
            if (value)
                lnkbtn_STP_Consumption.CssClass = "DisplayBlock";
            else
                lnkbtn_STP_Consumption.CssClass = "DisplayNone";
        }
    }

    protected void lnkbtn_STP_Consumption_Click(object sender, EventArgs e)
    {
        //Panel_consumption.Visible = true;
    }
    private void ConfirmConsumption()
    {
        if (Session["consumptionConfirm"] != null)
        {
            if (Session["consumptionConfirm"].ToString() != "")
            {
                STP_Consumption_Vis = true; // = STP_Print_Vis
                //if (GridView_consumption.Rows.Count == 0)
                //    STP_Print_Vis = false;

                //ModalPopupExtender_Consumption.Enabled = true;
                //Populate consumption grid
                GridviewConsumption();
                //Guid gOrderID = new Guid(Session["SuppOrderID"].ToString());
                Getconsumptiondate();
            }
            else
            {
                STP_Consumption_Vis = false; //= STP_Print_Vis
                //ModalPopupExtender_Consumption.Enabled = false;
                //Panel_consumption.Visible = false;
            }
        }
        else
        {
            STP_Consumption_Vis = false; // = STP_Print_Vis
            //ModalPopupExtender_Consumption.Enabled = false;
            //Panel_consumption.Visible = false;
        }
    }
    #endregion
    #region ADD NEW ITEMS
    public bool STP_AddNewItem_Vis
    {
        get
        {
            if (lnkbtn_STP_AddNewItem.CssClass == "DisplayNone")
                return false;
            else
                return true;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_STP_AddNewItem.CssClass = "DisplayBlock";
            else
                lnkbtn_STP_AddNewItem.CssClass = "DisplayNone";
        }
    }

    protected void lnkbtn_STP_AddNewItem_Click(object sender, EventArgs e)
    {
        try
        {
            //Panel_Addnewitem.Visible = true;
            AP_CleanFields();
        }
        catch (Exception EGridSelectindex)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "gv_Scan_SelectedIndexChanged", DateTime.Now.AddDays(0), EGridSelectindex.Message.ToString());
        }
    }
    #endregion

    /// <summary>
    /// Consumption can be done only after Shipment(which inserts ordercreateDispatch date in the tblimplantorder )
    /// </summary>
    private bool ItemsUnShipped()
    {
        // To make sure no other functionality is available if the items are shipped irrespective of role
        if (Session["consumptionConfirm"] != null)
        {
            if (Session["consumptionConfirm"].ToString() != "")
            {
                return false;
            }
            else
                return true;
        }
        else
            return true;
    }

    //#region PRINT CONSUMPTION
    //public bool STP_Print_Vis
    //{
    //    get
    //    {
    //        if (lnkbtn_STP_Print.CssClass == "DisplayNone")
    //            return false;
    //        else
    //            return true;
    //    }
    //    set
    //    {
    //        if (value)
    //            lnkbtn_STP_Print.CssClass = "DisplayBlock";
    //        else
    //            lnkbtn_STP_Print.CssClass = "DisplayNone";
    //    }
    //}

    //protected void lnkbtn_STP_Print_Click(object sender, EventArgs e)
    //{
    //    ////ModalPopupExtender_Consumption.Hide();
    //    //Panel_consumption.Visible = false;

    //    lnkbtn_CR_Print_Click(sender, e);
    //    //Panel_consumption.Visible = false;
    //}
    //#endregion
    #endregion

    #region POPULATE BOOKING
    #region BOOKING DETAILS
    protected void OrderDetails_CleanFields()
    {
        lbl_Status.Text = "";
        lbl_Patient.Text = "";
        lbl_PatientUR.Text = "";
        lbl_PatientType.Text = "";
        lbl_procedure.Text = "";
        lbl_ProcedureID.Text = "";
        lbl_loankit.Text = "";
        lbl_date.Text = "";
        lbl_surgeon.Text = "";
        lbl_bookingnumber.Text = "";
        lbl_HospitalName.Text = "";
        lbl_CaseType.Text = "";
    }
    private void OrderDetails_Populate()
    {
        try
        {
            RfidClass Orfidclass = new RfidClass();
            OrderDetails_CleanFields();

            string StrOrdno = Orfidclass.GetOrderno(ORDID.ToString());
            string StrDispatchDate = "";
            string StrConsumptionDate = "";
            string LblStatus = "";
            var status = from OrdStatus in Orfid.rpFn_OrderStatus(ORDID)
                         select OrdStatus;
            //lbl_Status.Text = status.ToString();
            foreach (var Vstatus in status)
            {
                LblStatus = Vstatus.ToString();
                lbl_Status.Text = lbl_Status.Text + LblStatus;
            }
            //lbl_Status.Text = LblStatus;

            var RfidOrder = Orfid.rp_implantConsumptionReportHeader(ORDID);

            var RfidOrderForReconcile = from Orders in Orfid.tblImplantOrders
                                        join Patients in Orfid.tblImplantPatients on Orders.ordPatientId equals Patients.ptId
                                        join Doctors in Orfid.tblImplantDoctors on Orders.ordDoctor equals Doctors.drId
                                        join Suppliers in Orfid.tblImplantSuppliers on Orders.ordSupplier equals Suppliers.supId
                                        join tblHosp in Orfid.tblImplantHospitals on Orders.ordOrgId equals tblHosp.hospOrgId
                                        where Orders.ordId == ORDID
                                        orderby Orders.ordDttm descending
                                        select new
                                        {
                                            OrderNO = Orders.ordNo,
                                            Patient = Patients.ptFamilyName,
                                            PatientUR = Patients.ptUR,
                                            PatientType = Patients.ptType,
                                            Doctor = Doctors.drTitle + " " + Doctors.drGivenName + " " + Doctors.drFamilyName,
                                            OrderDate = Orders.ordDttm,
                                            OperationName = Orders.ordOperationName,
                                            Supplier = Suppliers.supName,
                                            DispatchDate = Orders.ordCreateDispatch,
                                            ConsumptionDate = Orders.ordCreateConsumption,
                                            BookingNumber = Orders.ordBookingNumber,
                                            HospitalName = tblHosp.hospName + "," + tblHosp.hospCampus,
                                        };
            // CaseType = (int)Orders.tblImplantBookings.First<tblImplantBooking>().tblImplantBookingVersions.OrderByDescending<tblImplantBookingVersion,int>(bv => bv.revBookNo).First<tblImplantBookingVersion>().revCaseType
            foreach (var objOrder in RfidOrder)
            {
                lbl_Patient.Text = objOrder.ptFamilyName;
                lbl_PatientUR.Text = objOrder.ptUR;
                lbl_PatientType.Text = "";
                lbl_procedure.Text = objOrder.ordOperationName;
                lbl_ProcedureID.Text = objOrder.ordNo;
                lbl_loankit.Text = objOrder.supName;
                lbl_date.Text = objOrder.ordDttm.Value.ToString("dd/MMM/yyyy");//OrderStatus
                lbl_surgeon.Text = objOrder.Doctor;
                if (objOrder.ordCreateDispatch != null)
                    StrDispatchDate = objOrder.ordCreateDispatch.ToString();
                if (objOrder.ordCreateConsumption != null)
                    StrConsumptionDate = objOrder.ordCreateConsumption.ToString();
                lbl_bookingnumber.Text = objOrder.ordBookingNumber;
                lbl_HospitalName.Text = objOrder.HospitalName;
                lbl_CaseType.Text = objOrder.revCaseType;
            }
            GridView_orderDetails.DataSource = RfidOrderForReconcile;
            GridView_orderDetails.DataBind();
            if (Session["consumptionConfirm"] != null)
                Session.Remove("consumptionConfirm");
            Session["consumptionConfirm"] = StrDispatchDate.ToString();
        }
        catch (Exception ExReconcile)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "populateOrderdetails", DateTime.Now.AddDays(0), ExReconcile.Message.ToString());
        }
    }
    protected void GridView_orderDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Visible = false;
        }
    }
    #endregion
    private void Grid_ScanIN()
    {
        try
        {
            OrderDetails_Populate();
            Guid gOrgID = new Guid(Session["Se_ORGID"].ToString());
            string StrOrdno = ORfidclass.GetOrderno(ORDID.ToString());
            //var DSFillresults = Orfid.rp_implantFillResults(Request.QueryString["ordno"].ToString(), gOrgID);
            var DSFillresults = Orfid.rp_implantFillResults(StrOrdno, gOrgID);

            gv_Scan.DataSource = DSFillresults;

            for (int Colvisible = 14; Colvisible <= 24; Colvisible++)
            {
                gv_Scan.Columns[Colvisible].Visible = true;
            }
            gv_Scan.DataBind();
            // Page.RegisterStartupScript("CreateGridHeader", "<script>CreateGridHeader('divmt',gv_Scan','divhead');</script>");
            for (int ColInvisible = 14; ColInvisible <= 24; ColInvisible++)
            {
                gv_Scan.Columns[ColInvisible].Visible = false;
            }
        }
        catch (Exception EGridScan)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "Grid_ScanIN()", DateTime.Now.AddDays(0), EGridScan.Message.ToString());
        }
    }
    private void CommandReader()
    {
        try
        {
            int IntScanBatchID = 0;
            if (Session["ReaderID"] != null)
            {
                //if (Session["ReaderState"] == null)
                //{
                Guid GReaderID = new Guid(Session["ReaderID"].ToString());

                var oBatchId = Orfid.rp_implantCommandReader(GReaderID, ORfidclass.GetOrderno(ORDID.ToString()).ToString(),
                     Session["User"].ToString(),
                     int.Parse(Session["ReceiveType"].ToString()));
                Orfid.SubmitChanges();
                if (Session["ReceiveType"].ToString() != "0")
                {
                    foreach (var obatch in oBatchId)
                    {
                        IntScanBatchID = obatch.BatchID.Value;
                    }
                }
                Session["IntScanBatchID"] = IntScanBatchID;
                lbl_Reader.Text = Session["ReaderName"].ToString();
                Timer_gridscan.Enabled = true;
                //}
                //else
                //{
                //    lbl_confirmreaderstate.Text = "Reader is being Used by Another User"; 
                //    ModalPopupExtender4.Show();
                //}
            }
            else
            {
                MultipleReaders();
                if (Session["ReaderID"] == null)
                    ModalPopupExtender_multiplehosp.Show();
            }
        }
        catch (Exception ECommandreader)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "CommandReader()", DateTime.Now.AddDays(0), ECommandreader.Message.ToString());
        }
    }

    /// <summary>
    /// ReaderId must be obtainded from login.aspx
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Timer_gridscan_Tick(object sender, EventArgs e)
    {
        try
        {
            //pnl_ScannerFeedback.Visible = true;
            string StrThreadActive = "";
            string StrReadnewTags = "";
            string StrReadLastError = "";
            System.Guid GReaderID = new Guid(Session["ReaderID"].ToString());
            RFIDDataContext Orfid = new RFIDDataContext();

            var oReader = from ImplantReader in Orfid.tblImplantReaders
                          where ImplantReader.readId == GReaderID
                          select new
                          {
                              ThreadActive = ImplantReader.readThreadActive,
                              ReadNewTags = ImplantReader.readNewTags,
                              ReadLastError = ImplantReader.readLastError
                          };
            foreach (var Reader in oReader)
            {
                StrThreadActive = Reader.ThreadActive.ToString();
                StrReadnewTags = Reader.ReadNewTags.ToString();
                if (Reader.ReadLastError != null)
                    StrReadLastError = Reader.ReadLastError.ToString();
            }

            if (StrReadLastError == "")
            {
                if (Session["strNewTags"] != null)
                {
                    if (int.Parse(Session["strNewTags"].ToString()) != 0 || int.Parse(StrReadnewTags) != 0)
                    {
                        //  if (int.Parse(StrReadnewTags) != 0||)
                        //{
                        if (int.Parse(Session["strNewTags"].ToString()) != int.Parse(StrReadnewTags.ToString()))
                        {
                            //if (int.Parse(StrReadnewTags) - int.Parse(Session["strNewTags"].ToString()) == 0)
                            //    SF_lbl_ScanFeedback.Text = (Math.Round(float.Parse(SF_lbl_ScanFeedback.Text) * 2 / 3, 0, MidpointRounding.ToEven)).ToString();
                            //else
                            //    SF_lbl_ScanFeedback.Text = StrReadnewTags;

                            Grid_ScanIN();
                            Functionreadnewtags(StrReadnewTags);
                        }
                    }
                }
                Session["strNewTags"] = StrReadnewTags.ToString();
            }
            else
            {
                if (Session["strNewTags"] != null)
                    Session.Remove("strNewTags");
            }
        }
        catch (Exception ETimer)
        {
            Orfid.Usp_WebErrors("scan.aspx", "Timer_gridscan_Tick", DateTime.Now.AddDays(0), ETimer.Message.ToString());
        }
    }

    /* <summary>
     Cells[0]=rfidno Cells[1]=Part no Cells[2]=Description Cells[3]=Lot No Cells[4]=Expiry Cells[5]=SupplierASN
     Cells[6]=Received Cells[7]=Used Cells[8]=Dispatch Cells[9]=Bintag cells[10]=Comment
     Color Scheme Followed
     1. When Expiry Date is less than 90 days from the current date(It is highlighted as RED)
     * 2.When back Order and ASN are 0 the whole row is called header which has only rfid id , Part no and description
     * the whole row is highlighted in Khaki color
     * 3. When ASNQTY is 0 and backorder more than 0 then ASN column is gray and font is black else(bckcolor=LightGreen and Forecolor is DarkGreen)
     * 4.When colscan !=0 ,COLASN=0 and Colasn and colscan!=0 (backcolor=Lightgreen and forecolor=darkgreen)
     * 5.When any one of the columns lot number, partno,expiry are present but bo back order (col scan becomes Red,Red)
     * 6.else it becomes lightgray and black
     * 7.Back order is not present and colscan is present Lightyello and orange. 
     */
    protected void gv_Scan_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        try
        {
            int Intccolor = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex > -1)
                {
                    if (e.Row.Cells[17].Text != "&nbsp;")// Expiry
                    {
                        Intccolor = int.Parse(e.Row.Cells[17].Text);
                        e.Row.Cells[4].BackColor = (Intccolor == 0) ? e.Row.Cells[0].BackColor : ORfidclass.RFIDBackColor(Intccolor);
                        //e.Row.Cells[4].ForeColor = ORfidclass.RFIDForeColor(Intccolor);
                    }
                    if (e.Row.Cells[18].Text != "&nbsp;")// ASN
                    {
                        Intccolor = int.Parse(e.Row.Cells[18].Text);
                        e.Row.Cells[8].BackColor = (Intccolor == 0) ? e.Row.Cells[0].BackColor : ORfidclass.RFIDBackColor(Intccolor);
                        e.Row.Cells[8].ForeColor = ORfidclass.RFIDForeColor(Intccolor);

                    }
                    if (e.Row.Cells[19].Text != "&nbsp;")// Received
                    {
                        Intccolor = int.Parse(e.Row.Cells[19].Text);
                        e.Row.Cells[9].BackColor = (Intccolor == 0) ? e.Row.Cells[0].BackColor : ORfidclass.RFIDBackColor(Intccolor);
                        e.Row.Cells[9].ForeColor = ORfidclass.RFIDForeColor(Intccolor);

                    }
                    if (e.Row.Cells[20].Text != "&nbsp;")// Usage
                    {
                        Intccolor = int.Parse(e.Row.Cells[20].Text);
                        e.Row.Cells[10].BackColor = (Intccolor == 0) ? e.Row.Cells[0].BackColor : ORfidclass.RFIDBackColor(Intccolor);
                        e.Row.Cells[10].ForeColor = ORfidclass.RFIDForeColor(Intccolor);
                    }
                    if (e.Row.Cells[21].Text != "&nbsp;")//Dispatch
                    {
                        Intccolor = int.Parse(e.Row.Cells[21].Text);
                        e.Row.Cells[11].BackColor = (Intccolor == 0) ? e.Row.Cells[0].BackColor : ORfidclass.RFIDBackColor(Intccolor);
                        e.Row.Cells[11].ForeColor = ORfidclass.RFIDForeColor(Intccolor);
                    }

                    if (e.Row.Cells[6].Text == "0" && e.Row.Cells[7].Text == "0")
                    {
                        for (int k = 0; k < e.Row.Cells.Count; k++)
                            e.Row.Cells[k].BackColor = System.Drawing.Color.Cornsilk;
                    }
                }

                e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(gv_Scan, "Select$" + e.Row.RowIndex.ToString()));
            }
        }
        catch (Exception EGridrowdatabound)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "gv_Scan_RowDataBound1", DateTime.Now.AddDays(0), EGridrowdatabound.Message.ToString());
        }
    }

    protected void gv_Scan_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string strArg = "";
            //string StrOrdno = ORfidclass.GetOrderno(Request.QueryString["ordID"].ToString());
            if (gv_Scan.SelectedRow.Cells[22].Text == "&nbsp;")
            {
                strArg = "" + "," + gv_Scan.SelectedRow.Cells[24].Text + ";";
            }
            else
                //strArg = gv_Scan.SelectedRow.Cells[22].Text + "," + gv_Scan.SelectedRow.Cells[24].Text + ";" + StrOrdno;
                strArg = gv_Scan.SelectedRow.Cells[22].Text + "," + gv_Scan.SelectedRow.Cells[24].Text + ";";
            Timer_gridscan.Enabled = false;
            Session["Arg"] = strArg.ToString();
            Viewdetail_Detailswindow(strArg.ToString());
        }
        catch (Exception EGridSelectindex)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "gv_Scan_SelectedIndexChanged", DateTime.Now.AddDays(0), EGridSelectindex.Message.ToString());
        }
    }
    protected void gv_Scan_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    { }
    public override void VerifyRenderingInServerForm(Control gv_Scan)
    { }
    #endregion


    #region ADD ITEM
    protected void AP_CleanFields()
    {
        txt_partnumber.Text = "";
        txt_description.Text = "";
        txt_lotnumber.Text = "";
        txt_expiryDate.Text = "";
        txt_comment.Text = "";
    }
    protected void lnkbtn_AP_Ok_Click(object sender, EventArgs e)
    {
        try
        {
            DateTime DateExpiry = DateTime.Parse(txt_expiryDate.Text);
            Orfid.rp_implantManualAddLine(ORDID, IntBatchID, txt_partnumber.Text, txt_description.Text, txt_lotnumber.Text, DateExpiry, txt_comment.Text);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_Ok_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_Addnewitem.Hide();
        }
    }
    protected void lnkbtn_AP_Cancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender_Addnewitem.Hide();
    }
    #endregion

    #region ITEM DETAIL VIEWS
    #region ITEM DETAIL VIEW PROPERTIES
    /// <summary>
    /// Holds the current ASN No of a selected item in the item list (View Details Popup)
    /// </summary>
    public Nullable<int> ASNNO { get { return RfidClass.strNInt(Session["AsnNo"].ToString()); } set { if (value != null && value != -1) Session["AsnNo"] = value.ToString(); } }
    /// <summary>
    /// Holds the current Line No of a selected item in the item list (View Details Popup)
    /// </summary>
    public Nullable<int> LINNO { get { return RfidClass.strNInt(Session["LinNo"].ToString()); } set { if (value != null && value != -1) Session["LinNo"] = value.ToString(); } }
    #endregion

    #region VPD BUTTONS
    #region VPD CLOSE
    protected void lnkbtn_VPD_ViewCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender_ViewItem.Hide();
    }
    #endregion
    #region VPD SAVE
    public bool VPD_Save_Vis
    {
        get
        {
            return lnkbtn_VPD_Save.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_VPD_Save.Visible = true;
            else
                lnkbtn_VPD_Save.Visible = false;
        }
    }
    protected void lnkbtn_VPD_Save_Click(object sender, EventArgs e)
    {
        try
        {
            Nullable<DateTime> dtExpiry = null;
            if (txt_expiryDate.Text != "")
                dtExpiry = DateTime.Parse(txt_expiryDate.Text);
            Orfid.rp_implantManualUpdateLine(ORDID, IntBatchID, ASNNO, LINNO, txt_partno.Text, txt_desc.Text, txt_lotno.Text, dtExpiry, VPD_txt_YourComment.Text);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_VPD_Save_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_ViewItem.Hide();
        }
    }
    #endregion
    #region VPD DELETE
    public bool VPD_Delete_Vis
    {
        get
        {
            return lnkbtn_VPD_Delete.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_VPD_Delete.Visible = true;
            else
                lnkbtn_VPD_Delete.Visible = false;
        }
    }
    protected void lnkbtn_VPD_Delete_Click(object sender, EventArgs e)
    {
        try
        {
            Orfid.rp_implantManualRemoveLine(ORDID, IntBatchID, LINNO);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_VPD_Delete_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_ViewItem.Hide();
        }
    }
    #endregion

    #region VPD RECEIPT
    public bool VPD_ManualItemReceipted_Vis
    {
        get
        {
            return lnkbtn_VPD_ManualItemReceipted.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_VPD_ManualItemReceipted.Visible = true;
            else
                lnkbtn_VPD_ManualItemReceipted.Visible = false;
        }
    }
    protected void lnkbtn_VPD_ManualItemReceipted_Click(object sender, EventArgs e)
    {
        try
        {
            Nullable<DateTime> dtExpiry = null;
            if (lbl_Asn_Expiry.Text != "")
                dtExpiry = DateTime.Parse(lbl_Asn_Expiry.Text);
            Orfid.rp_implantManualAddLine(ORDID, IntBatchID, txt_partno.Text, txt_desc.Text, txt_lotno.Text, dtExpiry, VPD_txt_YourComment.Text);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_VPD_ManualItemReceipted_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_ViewItem.Hide();
        }
    }
    #endregion
    #region VPD UNRECEIPT
    public bool VPD_ManualItemUnReceipted_Vis
    {
        get
        {
            return lnkbtn_VPD_ManualItemUnreceipted.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_VPD_ManualItemUnreceipted.Visible = true;
            else
                lnkbtn_VPD_ManualItemUnreceipted.Visible = false;
        }
    }
    protected void lnkbtn_VPD_ManualItemUnreceipted_Click(object sender, EventArgs e)
    {
        try
        {
            Orfid.rp_implantManualRemoveLine(ORDID, IntBatchID, LINNO);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_Ok_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_ViewItem.Hide();
        }
    }
    #endregion

    #region VPD USED
    public bool VPD_ManualItemUsed_Vis
    {
        get
        {
            return lnkbtn_VPD_ManualItemUsed.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_VPD_ManualItemUsed.Visible = true;
            else
                lnkbtn_VPD_ManualItemUsed.Visible = false;
        }
    }
    protected void lnkbtn_VPD_ManualItemUsed_Click(object sender, EventArgs e)
    {
        try
        {
            char y = 'Y';
            Orfid.rp_implantManualUsage(ORDID, IntBatchID, LINNO, y, VPD_txt_YourComment.Text);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_VPD_ManualItemUsed_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_ViewItem.Hide();
        }
    }
    #endregion
    #region VPD UNUSED
    public bool VPD_ManualItemUnused_Vis
    {
        get
        {
            return lnkbtn_VPD_ManualItemUnused.Visible;
        }
        set
        {
            if (value && ItemsUnShipped())
                lnkbtn_VPD_ManualItemUnused.Visible = true;
            else
                lnkbtn_VPD_ManualItemUnused.Visible = false;
        }
    }
    protected void lnkbtn_VPD_ManualItemUnused_Click(object sender, EventArgs e)
    {
        try
        {
            char y = 'N';
            Orfid.rp_implantManualUsage(ORDID, IntBatchID, LINNO, y, VPD_txt_YourComment.Text);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "lnkbtn_VPD_ManualItemUnused_Click", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
        finally
        {
            ModalPopupExtender_ViewItem.Hide();
        }
    }
    #endregion

    #region VPD DISPATCH
    public bool VPD_ManualDispatch_Vis
    {
        get
        {
            return lnkbtn_VPD_ManualDispatch.Visible;
        }
        set
        {
            if (value)
            {
                lnkbtn_VPD_ManualDispatch.Visible = true;
            }
            else
            {
                lnkbtn_VPD_ManualDispatch.Visible = false;
            }
        }
    }
    protected void lnkbtn_VPD_ManualDispatch_Click(object sender, EventArgs e)
    {
        DispatchFunctionality('Y');
        ModalPopupExtender_ViewItem.Hide();
    }
    private void DispatchFunctionality(char Cdispatch)
    {
        try
        {
            Orfid.rp_implantManualDispatch(ORDID, IntBatchID, LINNO, Cdispatch, VPD_txt_YourComment.Text);
            Grid_ScanIN();
        }
        catch (Exception ElnkbtnOk)
        {
            Orfid.Usp_WebErrors("DetailsWindow.aspx", "DispatchFunctionality", DateTime.Now.AddDays(0), ElnkbtnOk.Message.ToString());
        }
    }
    #endregion
    #region VPD UNDISPATCH
    public bool VPD_ManualUndispatch_Vis
    {
        get
        {
            return lnkbtn_VPD_ManualUndispatch.Visible;
        }
        set
        {
            if (value)
            {
                lnkbtn_VPD_ManualUndispatch.Visible = true;
            }
            else
            {
                lnkbtn_VPD_ManualUndispatch.Visible = false;
            }
        }
    }
    protected void lnkbtn_VPD_ManualUndispatch_Click(object sender, EventArgs e)
    {
        DispatchFunctionality('N');
        ModalPopupExtender_ViewItem.Hide();
    }
    #endregion
    #endregion

    protected void VPD_CleanText()
    {
        txt_desc.Text = "";
        txt_tag0.Text = "";
        VPD_txt_ExistingComment.Text = "";
        VPD_txt_YourComment.Text = "";
        txt_partno.Text = "";
        txt_lotno.Text = "";
        VPD_txt_expiry.Text = "";
        lbl_Rec_Expiry.Text = "";
    }
    protected struct ItemStatusComplex
    {
        public bool supplierAsn, received, used, dispatched, manualyCreated;
        public ItemStatusComplex(bool v)
        {
            supplierAsn = v;
            received = v;
            used = v;
            dispatched = v;
            manualyCreated = v;
        }
    }
    private void Viewdetail_Detailswindow(string StrArguments)
    {
        try
        {
            string StrArg = StrArguments;
            string StrbatchID = "";
            ItemStatusComplex itemComplex = new ItemStatusComplex(false);

            //clear out intermittent text fields
            VPD_CleanText();
            int LinNo = -1;
            int AsnNo = -1;
            int.TryParse(StrArg.Substring(0, StrArg.IndexOf(',')), out LinNo);
            int.TryParse(StrArg.Substring(StrArg.IndexOf(',') + 1, StrArg.IndexOf(';') - 1 - StrArg.IndexOf(',')), out AsnNo);

            if (LinNo > 0)
                AsnNo = 0;

            LINNO = LinNo;
            ASNNO = AsnNo;

            // string StrOrderNO = StrArg.Substring(StrArg.IndexOf(';') + 1, StrArg.Length - StrArg.IndexOf(';') - 1);
            var TOrderID = from tblOrd in Orfid.tblImplantOrders
                           where (tblOrd.ordId == ORDID)
                           select new
                           {
                               ObatchID = tblOrd.ordBatchId
                           };

            foreach (var Vorder in TOrderID)
            {
                StrbatchID = Vorder.ObatchID.ToString();
            }

            //ASN
            Session["StrbatchID"] = StrbatchID.ToString();

            //for (int i = 0; i < gv_Scan.SelectedRow.Cells.Count; i++)
            //{
            //    System.Web.UI.WebControls.TableCell c = gv_Scan.SelectedRow.Cells[i];
            //    var a = c.Text;
            //}

            if (gv_Scan.SelectedRow.Cells[8].Text != "&nbsp;")
            {
                itemComplex.supplierAsn = true;
                var VimplantDetails = Orfid.rp_implantItemDetails(ORDID.ToString(), LinNo, AsnNo, "ASN");
                {
                    foreach (var Vimplantdetails_Asn in VimplantDetails)
                    {
                        txt_partno.Text = lbl_Asn_Partno.Text = Vimplantdetails_Asn.asnPartNo;
                        txt_lotno.Text = lbl_Asn_lotno.Text = Vimplantdetails_Asn.asnLotNo;
                        if (Vimplantdetails_Asn.asnExpiry != null)
                        {
                            string strExpiry = Vimplantdetails_Asn.asnExpiry.ToString();
                            DateTime dtExpiry = DateTime.Parse(strExpiry.ToString());
                            VPD_txt_expiry.Text = lbl_Asn_Expiry.Text = dtExpiry.ToString("dd/MMM/yyyy");
                        }
                        txt_tag0.Text = lbl_Asn_Rfidtag.Text = Vimplantdetails_Asn.asnRFID;
                        txt_desc.Text = Vimplantdetails_Asn.asnPartDesc;
                        ASN_lbl_user_time_details.Text = "ASN data for this item was read by: '" + Vimplantdetails_Asn.batchUser.ToString() + "' on " + Vimplantdetails_Asn.batchDttm + ".";
                    }
                }
            }
            else
            {
                lbl_Asn_Partno.Text = "";
                lbl_Asn_lotno.Text = "";
                lbl_Asn_Expiry.Text = "";
                lbl_Asn_Rfidtag.Text = "";
                ASN_lbl_user_time_details.Text = "This item was not detailed in the ASN file.";
                itemComplex.supplierAsn = false;
            }

            //Display Existing Comments
            string VimplantComments = "";
            Orfid.rp_implantGetComment(ORDID, AsnNo, LinNo, ref VimplantComments);
            if (VimplantComments == null)
            {
                VPD_txt_ExistingComment.Visible = false;
                VPD_txt_ExistingComment.Text = "";
            }
            else
            {
                VPD_txt_ExistingComment.Visible = true;
                VPD_txt_ExistingComment.Text = VimplantComments;
            }

            //Receipt 
            // if received column is null
            if (gv_Scan.SelectedRow.Cells[9].Text != "&nbsp;")
            {
                itemComplex.received = true;
                var VImplantdetails_Receive = Orfid.rp_implantItemDetails_Receipt(ORDID.ToString(), LinNo, AsnNo);
                if (VImplantdetails_Receive.ReturnValue != null)
                    foreach (var Vrec in VImplantdetails_Receive)
                    {
                        txt_partno.Text = lbl_Rec_PartNo.Text = Vrec.linPartNo;
                        txt_lotno.Text = lbl_Rec_lot.Text = Vrec.linLotNo;
                        lbl_Rec_rfidbin.Text = Vrec.linBinRfid;
                        lbl_Rec_RfidTag.Text = Vrec.linRfid; // txt_tag0.Text =
                        if (Vrec.linExpiry != null)
                        {
                            string strExpiry = Vrec.linExpiry.ToString();
                            DateTime dtExpiry = DateTime.Parse(strExpiry.ToString());
                            VPD_txt_expiry.Text = lbl_Rec_Expiry.Text = dtExpiry.ToString("dd/MMM/yyyy");
                        }
                        if (Vrec.linManual == 'Y' && txt_tag0.Text == "" && !itemComplex.supplierAsn)
                        {
                            itemComplex.manualyCreated = true;
                        }
                        txt_desc.Text = Vrec.linPartDesc;
                        RCP_lbl_user_time_details.Text = "Receipt data for this item was read by: '" + Vrec.batchUser.ToString() + "' on " + Vrec.batchDttm + ".";
                    }
            }
            else
            {
                lbl_Rec_Expiry.Text = "";
                lbl_Rec_lot.Text = "";
                lbl_Rec_rfidbin.Text = "";
                lbl_Rec_RfidTag.Text = "";
                lbl_Rec_PartNo.Text = "";
                RCP_lbl_user_time_details.Text = "This item has not been receipted yet.";
                itemComplex.received = false;
            }

            //Lock fields depending on Modifiability   --If the item was manually created and does not have an rfidtag number then details of the item can be modified
            //Detect Manual creation
            //LinNo <> null or asnlinNo <> null but not both = manually added / recepted respectively
            if (itemComplex.manualyCreated == true)
            {
                // Unlock & hide ASN tab
                txt_partno.Enabled = true;
                txt_lotno.Enabled = true;
                VPD_txt_expiry.Enabled = true;
                txt_desc.Enabled = true;
                lbl_Rec_PartNo.Enabled = true;
                TabContainer1.ActiveTabIndex = 1;
                Tab_Asn.Visible = false;
            }
            else
            {
                txt_partno.Enabled = false;
                txt_lotno.Enabled = false;
                VPD_txt_expiry.Enabled = false;
                txt_desc.Enabled = false;
                lbl_Rec_PartNo.Enabled = false;
                Tab_Asn.Visible = true;
                TabContainer1.ActiveTabIndex = 0;
            }

            //If Usage column is null it will bypassed to next
            var Vimplantdetails_Usage = Orfid.rp_implantItemDetails_Usage(ORDID.ToString(), LinNo, AsnNo);
            DG_Usage.DataSource = Vimplantdetails_Usage;
            DG_Usage.DataBind();

            if (gv_Scan.SelectedRow.Cells[10].Text != "&nbsp;")
                itemComplex.used = true;

            var Vimplantdetails_Dispatch = Orfid.rp_implantItemDetails_Dispatch(ORDID.ToString(), LinNo, AsnNo);
            DG_Dispatch.DataSource = Vimplantdetails_Dispatch;
            DG_Dispatch.DataBind();

            if (gv_Scan.SelectedRow.Cells[11].Text != "&nbsp;")
                itemComplex.dispatched = true;

            VPD_ToggleButtons(itemComplex);
            //Panel_viewitem.Visible = true;
            ModalPopupExtender_ViewItem.Show();
        }
        catch (Exception Eviewitem)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "ViewItem Details", DateTime.Now.AddDays(0), Eviewitem.Message.ToString());
        }
    }
    /// <summary>
    /// Turns on and off buttons appropriate for the the items stage in the booking.
    /// </summary>
    /// <param name="i">The configuration of the item clicked</param>
    protected void VPD_ToggleButtons(ItemStatusComplex i)
    {
        //CLOSE - Allways visible
        //SAVE - Turned off for some roles
        if (!SHIPPED)
            VPD_Save_Vis = true;
        else
            VPD_Save_Vis = false;

        //DELETE - Only Manually created items can be deleted
        if (i.manualyCreated && !i.supplierAsn && PROCESSLOANKIT && !SHIPPED)
            VPD_Delete_Vis = true;
        else
            VPD_Delete_Vis = false;

        //RECEIPT
        if (!i.received && !i.used && !i.dispatched && PROCESSLOANKIT && !SHIPPED)
            VPD_ManualItemReceipted_Vis = true;
        else
            VPD_ManualItemReceipted_Vis = false;

        //UNRECEIPT
        if (i.received && !i.used && !i.dispatched && PROCESSLOANKIT && !SHIPPED)
            VPD_ManualItemUnReceipted_Vis = true;
        else
            VPD_ManualItemUnReceipted_Vis = false;

        //USED
        if (i.received && !i.dispatched && !i.used && PROCESSLOANKIT && !SHIPPED)
            VPD_ManualItemUsed_Vis = true;
        else
            VPD_ManualItemUsed_Vis = false;

        //UNUSED
        if (i.used && !i.dispatched && PROCESSLOANKIT && !SHIPPED)
            VPD_ManualItemUnused_Vis = true;
        else
            VPD_ManualItemUnused_Vis = false;

        //DISPATCH
        if (i.received && !i.dispatched && PROCESSLOANKIT && !SHIPPED)
            VPD_ManualDispatch_Vis = true;
        else
            VPD_ManualDispatch_Vis = false;

        //UNDISPATCH
        if (i.dispatched && PROCESSLOANKIT && !SHIPPED)
            VPD_ManualUndispatch_Vis = true;
        else
            VPD_ManualUndispatch_Vis = false;
    }
    #endregion

    #region CONSUMPTION VIEW
    #region CONSUMPTION BUTTONS

    protected void lnkbtn_CR_Finalize_Click(object sender, EventArgs e)
    {
        try
        {
            var DispatchOrder = (from tblOrders in Orfid.tblImplantOrders
                                 where (tblOrders.ordId == ORDID && tblOrders.ordCreateDispatch != null)
                                 select tblOrders).Single();
            DispatchOrder.ordCreateConsumption = DateTime.Now.AddDays(0);
            DispatchOrder.ordConsumptionId = ACCID;
            Orfid.SubmitChanges();
            //lnkbtn_consum_Final.Visible = false;
            //lnkbtn_Save_consumptionreport.Visible = false;
            updateConsumption();
            //EnableconsumtionFinalFunction();
            GridviewConsumption();
            ConfirmConsumption();
        }
        catch (Exception ExConsumption)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_Consumption_Click", DateTime.Now.AddDays(0), ExConsumption.Message.ToString());
        }
    }
    protected void lnkbtn_CR_Save_Click(object sender, EventArgs e)
    {
        try
        {
            updateConsumption();
            //ModalPopupExtender_Consumption.Show();
        }
        catch (Exception ExSaveConsumptionreport)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_Save_consumptionreport_Click", DateTime.Now.AddDays(0), ExSaveConsumptionreport.Message.ToString());
        }
    }
    protected void lnkbtn_CR_Cancel_Click(object sender, EventArgs e)
    { }

    /// <summary>
    /// Understanding from Consumption functionality 
    /// Consumption cannot be enabled untill the Procedure is Dispatched(Dispatch date in the tblImplantOrder is entered)
    /// Once consumption field in the tblImplantOrder is filled , other fuctionality like scan and dispatch are disabled.
    /// OrdDispatchID=1; shows it is Dispatched
    /// ordConsumpationID=1;Shows it is Consumed and no furthur changes can be done on this order.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void lnkbtn_CR_Print_Click(object sender, EventArgs e)
    {
        try
        {
            updateConsumption(); //save the changes before printing

            string H = "H";
            string V = "V";

            string attachment = ORfidclass.GetOrderno(ORDID.ToString()).ToString() + ".Consumption.xls"; //"attachment; filename=" +
            //Prep Metadata for export
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(H, typeof(System.String)));
            dt.Columns.Add(new DataColumn(V, typeof(System.String)));

            DataRow dr0 = dt.NewRow();
            dr0[H] = "Patient Number";
            dr0[V] = lbl_PatientUR.Text;
            dt.Rows.Add(dr0);

            DataRow dr2 = dt.NewRow();
            dr2[H] = "Doctor/Surgeon";
            dr2[V] = lbl_surgeon.Text;
            dt.Rows.Add(dr2);

            DataRow dr5 = dt.NewRow();
            dr5[H] = "Implant Date";
            dr5[V] = lbl_date.Text;
            dt.Rows.Add(dr5);

            DataRow dr10 = dt.NewRow();
            dr10[H] = "Purchase Type";
            dr10[V] = lbl_PatientType.Text;
            dt.Rows.Add(dr10);

            DataRow dr11 = dt.NewRow();
            dr11[H] = "Hospital Name";
            dr11[V] = lbl_HospitalName.Text;
            dt.Rows.Add(dr11);

            DataRow dr12 = dt.NewRow();
            dr12[H] = "Case Type";
            dr12[V] = lbl_CaseType.Text;
            dt.Rows.Add(dr12);

            DataRow dr13 = dt.NewRow();
            dr13[H] = "";
            dr13[V] = "";
            dt.Rows.Add(dr13);

            DataRow dr1 = dt.NewRow();
            dr1[H] = "Procedure ID";
            dr1[V] = lbl_ProcedureID.Text;
            dt.Rows.Add(dr1);

            DataRow dr3 = dt.NewRow();
            dr3[H] = "Status";
            dr3[V] = lbl_Status.Text;
            dt.Rows.Add(dr3);

            DataRow dr4 = dt.NewRow();
            dr4[H] = "Patient=";
            dr4[V] = lbl_Patient.Text;
            dt.Rows.Add(dr4);

            DataRow dr6 = dt.NewRow();
            dr6[H] = "Loan Kit Supplier";
            dr6[V] = lbl_loankit.Text;
            dt.Rows.Add(dr6);

            DataRow dr7 = dt.NewRow();
            dr7[H] = "Procedure";
            dr7[V] = lbl_procedure.Text;
            dt.Rows.Add(dr7);

            DataRow dr8 = dt.NewRow();
            dr8[H] = "Supplier Ref";
            dr8[V] = lbl_bookingnumber.Text;
            dt.Rows.Add(dr8);

            DataRow dr9 = dt.NewRow();
            dr9[H] = "";
            dr9[V] = "";
            dt.Rows.Add(dr9);

            GridView gv = new GridView();
            gv.DataSource = dt;
            gv.DataBind();

            //prep Consumption Contents for Export
            GridView_consumption.AllowPaging = false;
            GridviewConsumption();          //reload the consumption view without paging
            GridView_consumption.HeaderRow.Cells.RemoveAt(4);
            GridView_consumption.HeaderRow.Cells.RemoveAt(4);
            GridView_consumption.HeaderRow.Cells.RemoveAt(4);
            foreach (GridViewRow row in GridView_consumption.Rows)
            {
                row.Cells.RemoveAt(4);
                row.Cells.RemoveAt(4);
                row.Cells.RemoveAt(4);
            }
            GridViewExportUtil.Export(attachment, GridView_consumption, gv);  //GridView_orderDetails
            GridView_consumption.AllowPaging = true;
            GridviewConsumption();
        }
        catch (System.Threading.ThreadAbortException lException)
        {
            return;
            // do nothing
        }
        catch (Exception ExReconcile)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_printConsumption_Click", DateTime.Now.AddDays(0), ExReconcile.Message.ToString());
        }
    }
    protected void gv_PrintConsumption_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex > -1)
                {
                    DropDownList ddl_consum_status = (DropDownList)e.Row.FindControl("ddl_consumption_status");
                    //TextBox txtfmiscode = (TextBox)e.Row.FindControl("txt_fmisCode");
                    TextBox txtcomments = (TextBox)e.Row.FindControl("txt_consumption_comments");
                    var ConsumStatus = from tblusage in Orfid.tblImplantReferences
                                       where
                                           tblusage.refKey == "Usage"
                                       orderby tblusage.refSortOrder
                                       select new
                                       {
                                           Statusvalue = tblusage.refSortOrder,
                                           statustext = tblusage.refValue
                                       };
                    ddl_consum_status.Items.Add("");
                    ddl_consum_status.AppendDataBoundItems = true;
                    ddl_consum_status.DataSource = ConsumStatus;
                    ddl_consum_status.DataTextField = "statustext";
                    ddl_consum_status.DataValueField = "Statusvalue";
                    ddl_consum_status.DataBind();
                    if (e.Row.Cells[4].Text != "&nbsp;")
                    {
                        ddl_consum_status.SelectedValue = e.Row.Cells[4].Text;
                    }
                    //if (e.Row.Cells[5].Text != "&nbsp;")
                    //{
                    //    txtfmiscode.Text = e.Row.Cells[5].Text;
                    //}
                    if (e.Row.Cells[6].Text != "&nbsp;")
                    {
                        txtcomments.Text = e.Row.Cells[6].Text;
                    }
                }
            }
        }
        catch (Exception ExGridviewRowdatabound)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "GridView_consumption_RowDataBound", DateTime.Now.AddDays(0), ExGridviewRowdatabound.Message.ToString());
        }
    }
    #endregion

    private void GridviewConsumption()
    {
        var dsViewconsumption = Orfid.rp_implantPopulateConsumption(ORDID);
        GridView_consumption.EnableViewState = true;
        GridView_consumption.DataSource = dsViewconsumption;
        GridView_consumption.Columns[4].Visible = true;
        GridView_consumption.Columns[5].Visible = true;
        GridView_consumption.Columns[6].Visible = true;
        GridView_consumption.Columns[7].Visible = true;
        GridView_consumption.Columns[8].Visible = true;
        GridView_consumption.DataBind();
        GridView_consumption.Columns[8].Visible = false;
        GridView_consumption.Columns[7].Visible = false;
        GridView_consumption.Columns[6].Visible = false;
        GridView_consumption.Columns[5].Visible = false;
        GridView_consumption.Columns[4].Visible = false;
        //EnableconsumtionFinalFunction();
    }
    private void Getconsumptiondate()
    {
        string strConsumptionDate = "";
        var dtconsumptiondate = from tblimpOrder in Orfid.tblImplantOrders
                                where tblimpOrder.ordId == ORDID
                                select tblimpOrder.ordCreateConsumption;
        foreach (var Vconsumptiondate in dtconsumptiondate)
        {
            strConsumptionDate = Vconsumptiondate.ToString();
        }
        if (strConsumptionDate != "")
        {
            lnkbtn_CR_Finalize.Visible = false;
            lnkbtn_CR_Save.Visible = false;
            if (GridView_consumption.Rows.Count > 0)
            {
                for (int IntRows = 0; IntRows < GridView_consumption.Rows.Count; IntRows++)
                {
                    DropDownList ddl_consum_status = (DropDownList)GridView_consumption.Rows[IntRows].FindControl("ddl_consumption_status");
                    // TextBox txtfmiscode = (TextBox)GridView_consumption.Rows[Introw].FindControl("txt_fmisCode");
                    TextBox txtcomments = (TextBox)GridView_consumption.Rows[IntRows].FindControl("txt_consumption_comments");
                    ddl_consum_status.Enabled = false;
                    txtcomments.Enabled = false;
                }
            }
        }
    }
    private void updateConsumption()
    {
        try
        {
            if (GridView_consumption.Rows.Count > 0)
            {
                RFIDDataContext orfidconsum = new RFIDDataContext();
                {
                    for (int Introw = 0; Introw < GridView_consumption.Rows.Count; Introw++)
                    {
                        DropDownList ddl_consum_status = (DropDownList)GridView_consumption.Rows[Introw].FindControl("ddl_consumption_status");
                        // TextBox txtfmiscode = (TextBox)GridView_consumption.Rows[Introw].FindControl("txt_fmisCode");
                        TextBox txtcomments = (TextBox)GridView_consumption.Rows[Introw].FindControl("txt_consumption_comments");
                        var tblConsumption = orfidconsum.tblImplantConsumptions.Where(tbcon => tbcon.conOrderId == ORDID && tbcon.conLineNo == int.Parse(GridView_consumption.Rows[Introw].Cells[1].Text.ToString()));
                        foreach (var rowconsum in tblConsumption)
                        {
                            if (ddl_consum_status.SelectedValue != "")
                                rowconsum.conStatus = int.Parse(ddl_consum_status.SelectedValue.ToString());
                            //if(txtfmiscode.Text!="")
                            //rowconsum.conFMISCode=txtfmiscode.Text;
                            if (txtcomments.Text != "")
                                rowconsum.conComment = txtcomments.Text;
                        }
                    }
                }
                orfidconsum.SubmitChanges();
                GridviewConsumption();
                //lbl_consumption_confirm.Text = "Consumption Records Added Successfully";
            }
        }
        catch (Exception ExSaveconsumptionreport)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "updateConsumption", DateTime.Now.AddDays(0), ExSaveconsumptionreport.Message.ToString());
        }
    }

    /// <summary>
    /// This function is to check whether FMIS Codes for all the rows have been added if not consumption report button will not be enabled.
    /// </summary>
    private void EnableconsumtionFinalFunction()//Changes made to the function
    {
        try
        {
            string Strconsumdate = "";
            var Strconsumption = from tblcons in Orfid.tblImplantOrders
                                 where tblcons.ordId == ORDID
                                 select new { Consumptiondate = tblcons.ordCreateConsumption };
            foreach (var condate in Strconsumption)
            {
                Strconsumdate = condate.Consumptiondate.ToString();
            }
            if (Strconsumdate != "")
            {
                lnkbtn_CR_Finalize.Visible = false;
                lnkbtn_CR_Save.Visible = false;
                if (GridView_consumption.Rows.Count > 0)
                {
                    for (int IntRows = 0; IntRows < GridView_consumption.Rows.Count; IntRows++)
                    {
                        DropDownList ddl_consum_status = (DropDownList)GridView_consumption.Rows[IntRows].FindControl("ddl_consumption_status");
                        // TextBox txtfmiscode = (TextBox)GridView_consumption.Rows[Introw].FindControl("txt_fmisCode");
                        TextBox txtcomments = (TextBox)GridView_consumption.Rows[IntRows].FindControl("txt_consumption_comments");
                        ddl_consum_status.Enabled = false;
                        txtcomments.Enabled = false;
                    }
                }
            }
        }
        catch (Exception ExEnableconsumtionFinalFunction)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "EnableconsumtionFinalFunction", DateTime.Now.AddDays(0), ExEnableconsumtionFinalFunction.Message.ToString());
        }
    }

    protected void GridView_consumption_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridView_consumption.PageIndex = e.NewPageIndex;
            updateConsumption();
            EnableconsumtionFinalFunction();
            ModalPopupExtender_Consumption.Show();
        }
        catch (Exception ExGridviewConsumption)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "GridView_consumption_PageIndexChanging1", DateTime.Now.AddDays(0), ExGridviewConsumption.Message.ToString());
        }
    }
    protected void GridView_consumption_PageIndexChanged(object sender, EventArgs e)
    { }
    protected void GridView_consumption_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex > -1)
                {
                    DropDownList ddl_consum_status = (DropDownList)e.Row.FindControl("ddl_consumption_status");
                    //TextBox txtfmiscode = (TextBox)e.Row.FindControl("txt_fmisCode");
                    TextBox txtcomments = (TextBox)e.Row.FindControl("txt_consumption_comments");
                    var ConsumStatus = from tblusage in Orfid.tblImplantReferences
                                       where
                                           tblusage.refKey == "Usage"
                                       orderby tblusage.refSortOrder
                                       select new
                                       {
                                           Statusvalue = tblusage.refSortOrder,
                                           statustext = tblusage.refValue
                                       };
                    ddl_consum_status.Items.Add("");
                    ddl_consum_status.AppendDataBoundItems = true;
                    ddl_consum_status.DataSource = ConsumStatus;
                    ddl_consum_status.DataTextField = "statustext";
                    ddl_consum_status.DataValueField = "Statusvalue";
                    ddl_consum_status.DataBind();
                    if (e.Row.Cells[4].Text != "&nbsp;")
                    {
                        ddl_consum_status.SelectedValue = e.Row.Cells[4].Text;
                    }
                    //if (e.Row.Cells[5].Text != "&nbsp;")
                    //{
                    //    txtfmiscode.Text = e.Row.Cells[5].Text;
                    //}
                    if (e.Row.Cells[6].Text != "&nbsp;")
                    {
                        txtcomments.Text = e.Row.Cells[6].Text;
                    }
                }
            }
        }
        catch (Exception ExGridviewRowdatabound)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "GridView_consumption_RowDataBound", DateTime.Now.AddDays(0), ExGridviewRowdatabound.Message.ToString());
        }
    }
    #endregion

    #region MULTIPLE READERS
    private void MultipleReaders()
    {
        try
        {
            int IntReaders = 0;
            string StrReaderState = "";
            string StrMultiReaderID = "";
            string StrReaderName = "";
            Guid Gorg = new Guid(Session["Se_ORGID"].ToString());
            var tblImpReader = from tbReader in Orfid.tblImplantReaders
                               where tbReader.readOrgId == Gorg
                               select new
                               {
                                   ReaderID = tbReader.readId,
                                   ReaderWorkStation = tbReader.readWorkstation,
                                   ReaderName = tbReader.readName,
                                   ReaderState = tbReader.readState
                               };
            IntReaders = tblImpReader.Count();
            if (IntReaders == 1)
            {
                foreach (var Readers in tblImpReader)
                {
                    StrMultiReaderID = Readers.ReaderID.ToString();
                    StrReaderState = Readers.ReaderState.ToString();
                    StrReaderName = Readers.ReaderName.ToString();
                }
                if (int.Parse(StrReaderState) != 0)
                {
                    ModalPopupExtender_Readerstate.Show();
                }
                else
                {
                    Session["ReaderID"] = StrMultiReaderID.ToString();
                    Session["ReaderName"] = StrReaderName;
                    CommandReader();
                }
            }
            List_reader.Items.Clear();
            List_reader.DataSource = tblImpReader;
            List_reader.DataTextField = "ReaderName";
            List_reader.DataValueField = "ReaderID";
            List_reader.DataBind();
        }
        catch (Exception exMultipleReaders)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "Multiplereaders", DateTime.Now.AddDays(0), exMultipleReaders.Message.ToString());
        }
    }
    protected void lnkbtn_SubmitReaders_Click(object sender, EventArgs e)
    {
        try
        {
            string Strreaderstate = "";

            if (List_reader.SelectedValue != "")
            {
                //Session["ReaderID"] = ddl_Readers.SelectedValue.ToString();
                Guid gReadID = new Guid(List_reader.SelectedValue.ToString());
                //To check the reader state
                Guid Gorg = new Guid(Session["Se_ORGID"].ToString());
                var tbImpReader = from tbReader in Orfid.tblImplantReaders
                                  where tbReader.readOrgId == Gorg && tbReader.readId == gReadID
                                  select new
                                  {
                                      ReaderState = tbReader.readState
                                  };
                foreach (var tbreader in tbImpReader)
                {
                    Strreaderstate = tbreader.ReaderState.ToString();
                }
                if (Strreaderstate != "0" && Strreaderstate != "")
                {
                    ModalPopupExtender_Readerstate.Show();
                }
                else
                {
                    Session["ReaderID"] = List_reader.SelectedValue.ToString();
                    Session["ReaderName"] = List_reader.SelectedItem.Text;
                    CommandReader();
                }
            }
        }
        catch (Exception exSubmitReaders)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnkbtn_Submitreaders_Click", DateTime.Now.AddDays(0), exSubmitReaders.Message.ToString());
        }
    }
    protected void lnk_reader_select_Click(object sender, EventArgs e)
    {
        try
        {
            if (List_reader.SelectedValue != "")
            {
                Session["ReaderID"] = List_reader.SelectedValue.ToString();
                Session["ReaderName"] = List_reader.SelectedItem.Text;
            }
            else
            {
                Session["ReaderID"] = List_reader.Items[0].Value.ToString();
                Session["ReaderName"] = List_reader.Items[0].Text;
                //RFID Orfid = new RFID();
                //Orfid.StopReader();
            }
            CommandReader();
        }
        catch (Exception exReaderselect)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnk_reader_select_Click", DateTime.Now.AddDays(0), exReaderselect.Message.ToString());
        }
    }
    protected void lnk_reader_deselect_Click(object sender, EventArgs e)
    {
        try
        {
            STP_ImgUsage_Active_Toggle(false);
            STP_ImgDispatch_Active_Toggle(false);
            STP_ImgScanIN_Active_Toggle(false);
        }
        catch (Exception exReaderdeselect)
        {
            Orfid.Usp_WebErrors("Scan.aspx", "lnk_reader_deselect_Click", DateTime.Now.AddDays(0), exReaderdeselect.Message.ToString());
        }
    }
    #endregion
}